FROM nikolaik/python-nodejs:python3.9-nodejs15-alpine

RUN node --version
RUN npm --version

WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip

COPY alexandria/requirements.txt ./

RUN \
    apk add --no-cache postgresql-libs && \
    apk add --no-cache --virtual .build-deps gcc python3-dev musl-dev postgresql-dev && \
    apk add jpeg-dev zlib-dev libjpeg && \
    apk add --update --no-cache g++ gcc libxslt-dev

RUN pip install -r requirements.txt && \
    apk --purge del .build-deps

COPY alexandria/. .

# env vars
COPY alexandria/config/settings_local.example.py config/settings_local.py

# ssh git
ARG SSH_PRIVATE_KEY
ARG SSH_SERVER_HOSTKEYS
RUN mkdir /root/.ssh/
RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa
RUN echo "${SSH_SERVER_HOSTKEYS}" > /root/.ssh/known_hosts
RUN chmod 0600 /root/.ssh/id_rsa
RUN pwd && ls -al /root/.ssh/

# build statics
WORKDIR /usr/src/app/client

RUN NODE_OPTIONS="--max-old-space-size=8192" npm i
RUN NODE_OPTIONS="--max-old-space-size=8192" npm run build
RUN NODE_OPTIONS="--max-old-space-size=8192" npm run build:ssr
RUN npm install -g pm2

WORKDIR /usr/src/app
RUN python manage.py collectstatic --no-input

# DEBUG
RUN pwd && ls -al

CMD ["sh", "./start.sh"]
