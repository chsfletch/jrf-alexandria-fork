# New Alexandria

[![pipeline status](https://gitlab.com/center-for-hellenic-studies/alex/badges/develop/pipeline.svg)](https://gitlab.com/center-for-hellenic-studies/alex/commits/develop)
[![coverage report](https://gitlab.com/center-for-hellenic-studies/alex/badges/develop/coverage.svg)](https://gitlab.com/center-for-hellenic-studies/alex/commits/develop)

Peer-reviewed, Open, and Accessible Philology for the Information Age. The New Alexandria project enables users to publish open and collaborative digital editions, commentaries, and translations for pre-medieval works in 14 languages, reusing texts from the Perseus Project, Open Greek and Latin, and CLTK.


## Prerequisites

* Docker
* docker-compose

## Hosts file for multitenancy

Ideally, multitenancy should be handled through the nginx.conf.

To develop with multitenancy features, please add these entries to your the hosts file on your development machine, such as at `/etc/hosts`:

```
127.0.0.1       alexandria.local
127.0.0.1       homer.alexandria.local
127.0.0.1       test.alexandria.local
127.0.0.1       [other-subdomains-you-want-to-test].alexandria.local
```

You will want to configure the nginx.conf for handling these. [TODO -- describe how to configure nginx]

## Environment variables

Copy the `.env.example` file in the client directory to `.env` to set environment variables for the React frontend. To connect to different versions of the API for development (i.e. production or staging), modify the `REACT_APP_GRAPHQL_SERVER` variable, for instance.

The `.env.example` will work with a fresh clone to get you started developing with the staging API.


## Development with Docker

If you have a recent version of Docker and docker-compose, getting started
is as simple as running `docker-compose up`.

If you want to prepopulate your development database with a recent postgres
dump, get its container's ID using `docker ps`:

```
CONTAINER ID   IMAGE            COMMAND                  CREATED          STATUS         PORTS                    NAMES
2575f6150ffe   nginx            "/docker-entrypoint.…"   2 minutes ago    Up 2 minutes   0.0.0.0:8080->80/tcp     alexandria_nginx_1
29a916c79458   alexandria_web   "python manage.py ru…"   2 minutes ago    Up 2 minutes   0.0.0.0:8000->8000/tcp   alexandria_web_1
0c55c9082b09   postgres         "docker-entrypoint.s…"   23 minutes ago   Up 2 minutes   0.0.0.0:5432->5432/tcp   alexandria_db_1
```

You want the container ID for `postgres`. (These IDs are just examples.)

Then, with the Docker container running, in another terminal tab run ` cat
alexandria.sql | docker exec -i {YOUR ID HERE} psql -U postgres -d alexandria`.


### Environment variables

`docker-compose` will automatically pick up environment variables in a .env
file in the same directory, but you they will still need to be assigned to
variables for each container. See the docker-compose.yml file for examples.

## Development (deprecated)

To get started developing, first clone the project repository, create a virtual environment, and install the project python and javascript packages:

```
$ git clone git@gitlab.com:center-for-hellenic-studies/alex.git
$ virtualenv venv
$ source venv/bin/activate
$ cd alexandria
$ pip install -r requirements.txt
$ cd ./client
$ npm i
```

(Note: you may need to include `NODE_OPTIONS=--max_old_space_size=8192` if you run into memory issues.)

Configure the backend django application's local settings by copying `./config/settings_local.example.py` to a `./config/settings_local.py` file with credentials for your local environment.

Next, start both the django python and node frontend processes simultaneously in two separate terminals.

Apply database migrations and start the Django development server:
```
(in a terminal with your virtualenv active)
$ python manage.py migrate
$ python manage.py runserver
```

And in a separate terminal, start the frontend:

```
$ npm start
```

To enable server-side rendering of the frontend Reading Environment, open another terminal and run:

```
$ npm run dev:ssr
```

You will need to set the environment variable `RENDERING_SERVER_URL` in settings_local.py. By default, the SSR server will run at http://localhost:8082/.

Open a browser to http://alexandria.local:8000/ to see the Alexandria homepage; open a browser to http://homer.alexandria.local:8000/ to see the Homer Commentary on Alexandria if you're connected to the staging API.
