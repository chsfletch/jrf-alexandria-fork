New Alexandria Foundation
------

## Initial setup

This application is designed to be run with `docker-compose` so that
development mirrors production as closely as possible.

If you are starting from scratch, you will just need to modify your .env and
nginx.conf files to provide the credentials you want to use. It's probably
easiest if you keep things mostly the same as they are in the examples:

```
$ cp .env.example .env
$ cp nginx.conf.example nginx.conf
```

If you don't have a local version of the GraphQL Textserver running, make sure
that you adjust `TEXTSERVER_URL` to point to the remote version that you want
to use. You'll also need to make sure that `HOSTNAME` is properly configured as
an alias to `localhost` or `0.0.0.0` in your /etc/hosts file.

The first time you run `docker-compose up`, it might take a few tries for the
`web` service to connect to the `db` service --- just be patient, and the
connection should sort itself out.


### Migrations and data

1. `docker exec -it <your_container_id> bash`
1. `python manage.py migrate`
1. `python manage.py loaddata <your_data_file>.json`

You will need access to the `alexandria_web` Docker container. Get the
container ID for the `alexandria_web` container by running `docker ps`, then
attach to it with `docker exec -it <your_container_id> bash`.

At this point, you'll need some data to work with. The easiest way to load data
into the database is via `python manage.py loaddata <your_data_file>.json`. If
you're starting from scratch, you won't have any data to work with, so ask
someone else on the team for a seed fixture, which can be generated with
`python manage.py dumpdata -v 1 -o <your_data_file>.json -e contenttypes`. Be
sure to include the `-e contenttypes` flag, otherwise the data won't load
properly.

NOTE: Make sure that the JSON migration file is in this directory. If it isn't,
you can always `docker cp` it later, but it's easiest if you already have it
handy.

Now that you're in a bash shell in the container, you can run `python manage.py
migrate`. Then simply load or dump the data that you need.


### Starting from a non-Docker version

If you're starting from the non-Docker version of local development, you'll
want to dump the data _before_ attaching to the `alexandria_web` container.
Thus, making sure you've activated your `venv` and have the DB configured to
connect to your existing Postgres instance, run `python manage.py dumpdata -v 1
-o <your_data_file>.json -e contenttypes` as above.

Then attach to the `alexandria_web` container (get its ID with `docker ps`):
`docker exec -it <your_container_id> bash`, run the migrations if you need to,
and load your data `python manage.py loaddata <your_data_file>.json`.


### Wait, why aren't we using Docker's fancy /docker-entrypoint-initdb.d/?

In theory, this is a great backup option that would allow us to skip a lot of
the initialization steps above. In practice, it adds an additional layer of
complexity, and it will create auto-incrementing conflicts with many (most?) of
the models in the DB. We can avoid those issues by using Django's fixtures as
described above.


## Setting up the SSR server and the client-side JavaScript build

To avoid conflicts between different architectures, we need to keep the host
and container `node_modules` separate. In development, this means that we need
to run `npm i` inside the `ssr` or `client` container, like so:

```
$ docker-compose run --rm ssr npm i
```

Then you can run `docker-compose up` and start the SSR server alongside the
Django API, Postgres DB, and Nginx reverse proxy.

Note that because the `ssr` and `client` containers share the `nodemodules`
volume, you only have to run this once whenever package.json changes.

## Setting up the textserver

You might need to adjust the variables that point to the textserver. As of
right now, these are `TEXTSERVER_URL`, which should be defined from the
container context (i.e., using `host.docker.internal` to point to the host
machine's `localhost`); and `REACT_APP_GRAPHQL_URL`, which should be defined
from the host context (i.e., just `localhost`). If you're using a remote
server, you can use the same URL in both cases.

At some point we might want to consider making the textserver a part of this
Docker cluster, but it's a fairly heavy application for local development.
