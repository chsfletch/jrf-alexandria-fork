import json

from django import template

register = template.Library()


@register.filter
def parse_item_desc(description):
    result_desc = ''
    item_desc_json = description or '{}'
    try:
        raw_draft_content = json.loads(item_desc_json)
        if "blocks" in raw_draft_content:
            for block in raw_draft_content["blocks"]:
                result_desc += block['text']
    except Exception:
        result_desc = description
    return result_desc


@register.filter
def get_color_rgb(color_json):
    result_rgb = None
    try:
        color = json.loads(color_json[0])
        if 'color' in color:
            result_rgb = color['color']
    except Exception:
        return result_rgb
    return result_rgb


@register.filter
def in_list(value, the_list_str):
    value = str(value)
    the_list = the_list_str.split(',')
    return value in the_list


@register.filter
def first_image_file_url(files):
    image_url = '/static/images/default.jpg'
    file = files.filter(mime_type__icontains='image').first()
    if file:
        if hasattr(file, 'filepath') and file.filepath.url.endswith("gif"):
            image_url = "//iiif-orpheus.s3.amazonaws.com/{}".format(file.filename)
        else:
            image_url = "//iiif.orphe.us/{}/full/,400/0/default.jpg".format(file.filename)

    return image_url
