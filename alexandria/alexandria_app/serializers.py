from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.validators import UnicodeUsernameValidator
from rest_framework import serializers
from rest_framework.response import Response

from .lib.get_project_from_request import get_project_from_request

from .models import Comment, RevisionComment, UserText, Tag, RevisionUserText, Project, CustomUser


class RevisionCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = RevisionComment
        fields = ['id', 'text', 'text_raw',
                  'title', 'created_at', 'updated_at']
        extra_kwargs = {'id': {'read_only': False, 'required': False}}


class CommenterSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['username', 'first_name', 'last_name', 'picture']


class TagSerializer(serializers.HyperlinkedModelSerializer):
    # FIXME: comments = RevisionCommentSerializer(many=True, required=False)

    class Meta:
        model = Tag
        fields = ['name', 'comments', 'id']


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    revisions = RevisionCommentSerializer(
        many=True,
    )
    tags = TagSerializer(many=True)
    commenters = CommenterSerializer(many=True, required=False)

    class Meta:
        model = Comment
        fields = ['id', 'urn', 'privacy', 'revisions', 'tags', 'commenters']

    def create(self, validated_data):
        request = self.context.get("request")
        project = get_project_from_request(request)
        validated_data['project_id'] = project.id
        revisions = validated_data.pop('revisions')
        tags = validated_data.pop('tags')

        # defaults to current user
        if not request.user.id:
            raise serializers.ValidationError("User not logged in.")
        commenters = [request.user]
        # commenters = validated_data.pop('commenters')

        comment_new = Comment.objects.create(**validated_data)
        for revision in revisions:
            RevisionComment.objects.create(comment=comment_new, **revision)

        for tag in tags:
            instance, _created = Tag.objects.get_or_create(name=tag.get(
                'name'), project=project)
            comment_new.tags.add(instance)
            instance.comments.add(comment_new)

        for commenter in commenters:
            comment_new.commenters.add(commenter)

        return comment_new

    def update(self, instance, validated_data):
        request = self.context.get("request")

        # get input revisions data
        revisions_data = validated_data.pop('revisions')
        tags = validated_data.pop('tags')

        # defaults to current user
        commenters = [request.user]
        # commenters = validated_data.pop('commenters')

        # update comment
        instance.urn = validated_data['urn']
        instance.privacy = validated_data['privacy']
        instance.save()

        # create new revision for non-existing input revisions
        for revision in revisions_data:
            revision_id = revision.get('id', None)
            if not revision_id:
                # create new revision and add to instance
                revision_new = RevisionComment(
                    comment=instance,
                    text=revision['text'],
                    text_raw=revision['text_raw'],
                    title=revision['title'],
                )
                revision_new.save(force_insert=True)

        for tag in tags:
            t, _created = Tag.objects.get_or_create(name=tag.get(
                'name'), project_id=instance.project_id)
            instance.tags.add(t)
            t.comments.add(instance)

        for commenter in commenters:
            instance.commenters.add(commenter)

        return instance


class RevisionUserTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = RevisionUserText
        fields = ['id', 'text', 'text_raw',
                  'title', 'created_at', 'updated_at']
        extra_kwargs = {'id': {'read_only': False, 'required': False}}


class UserTextSerializer(serializers.HyperlinkedModelSerializer):
    revisions = RevisionUserTextSerializer(
        many=True,
    )
    # selected_revision = RevisionUserTextSerializer(read_only=True)
    selected_revision_id = serializers.IntegerField(
        allow_null=True,
        required=False)
    latest_revision_text = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = UserText
        fields = ['id', 'urn', 'content_type', 'privacy',
                  'revisions', 'latest_revision_text', 'selected_revision_id']
        extra_kwargs = {'selected_revision_id': {'required': False}}

    def get_latest_revision_text(self, usertext):
        latest_revision = usertext.selected_revision
        if latest_revision is None:
            latest_revision = usertext.revisions.first()
        return latest_revision.text

    def create(self, validated_data):
        request = self.context.get("request")
        project = get_project_from_request(request)
        validated_data['project_id'] = project.id
        revisions = validated_data.pop('revisions')
        # perform update when urn with project_id exists
        user_text = None

        try:
            user_text_exist = UserText.objects.get(
                urn=validated_data['urn'],
                project=project,
                content_type=validated_data['content_type'])
            if (user_text_exist):
                for revision in revisions:
                    revision_id = revision.get('id', None)
                    if not revision_id:
                        # create new revision and add to instance
                        revision_new = RevisionUserText(
                            user_text=user_text_exist,
                            text=revision['text'],
                            text_raw=revision.get('text_raw', None),
                            title=revision.get('title', None),
                        )
                        revision_new.save(force_insert=True)
                user_text = user_text_exist
        except ObjectDoesNotExist:
            user_text_new = UserText.objects.create(**validated_data)
            for revision in revisions:
                RevisionUserText.objects.create(
                    user_text=user_text_new, **revision)
            user_text = user_text_new

        return user_text

    def update(self, instance, validated_data):
        # get input revisions data
        revisions_data = validated_data.pop('revisions')

        # update comment
        instance.urn = validated_data['urn']
        instance.privacy = validated_data['privacy']
        instance.content_type = validated_data['content_type']
        instance.save()

        if validated_data['selected_revision_id'] is not None:
            try:
                # make sure that this revision exists
                instance.revisions.get(
                    id=validated_data['selected_revision_id'])
                instance.selected_revision_id = validated_data['selected_revision_id']
                instance.save()
            except ObjectDoesNotExist:
                pass

        # create new revision for non-existing input revisions
        for revision in revisions_data:
            revision_id = revision.get('id', None)
            if not revision_id:
                # create new revision and add to instance
                revision_new = RevisionUserText(
                    user_text=instance,
                    text=revision['text'],
                    text_raw=revision.get('text_raw', None),
                    title=revision.get('title', None),
                )
                revision_new.save(force_insert=True)
                # if a revision is newly created, assume that
                # it should also be the selected revision
                instance.selected_revision_id = revision_new.id
                instance.save()

        return instance


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Project
        # TODO: admins, commenters, editors
        fields = ['id', 'hostname', 'title', 'privacy', 'about_html']
