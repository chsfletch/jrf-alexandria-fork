# Generated by Django 2.2.6 on 2020-01-20 08:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alexandria_app', '0005_auto_20200113_0950'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='picture',
            field=models.ImageField(blank=True, upload_to='profile_images'),
        ),
    ]
