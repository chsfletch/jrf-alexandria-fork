# Generated by Django 2.2.6 on 2020-02-18 14:32

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alexandria_app', '0017_auto_20200218_2230'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='commenters',
            field=models.ManyToManyField(related_name='commenters', to=settings.AUTH_USER_MODEL),
        ),
    ]
