COMMENTARY_COMMENTERS = [{
        'hostname': 'homer',
        'commenters': [{
                'name': "Anita Nikkanen",
                'slug': "anita-nikkanen",
                'avatarUrl': "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ%20/%20anita_nikkanen.jpeg",
                'bio': "Anita Nikkanen is a Research Fellow in Classical Philology at the Center for Hellenic Studies. Her research focuses on Homeric epic and Augustan poetry, and she has published on Ovid, Propertius, Tibullus, Virgil, and the Homeric Odyssey."
            }, {
                'name': "Casey Dué",
                'slug': "casey-due",
                'avatarUrl': "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ%20/%20casey_due.jpg",
                'bio': "Casey Dué is Professor and Director of Classical Studies at the University of Houston. Publications include Homeric Variations on a Lament by Briseis (Lanham, MD, 2002), The Captive Woman’s Lament in Greek Tragedy (Austin, 2006) and (co-authored with Mary Ebbott) Iliad 10 and the Poetics of Ambush (Washington, DC, 2010). She is co-editor with Mary Ebbott of the Homer Multitext project (http://www.homermultitext.org)."
            }, {
                'name': "Corinne Pache",
                'slug': "corinne-pache",
                'avatarUrl': "https://ahcip.chs.harvard.edu/images/default_user.jpg",
                'bio': "Corinne Pache is a Professor in the Classical Studies department at Trinity, where she started teaching in the fall of 2009. Her research focuses on Greek archaic poetry and the modern reception of ancient epic. Recent publications include 'Go Back to Your Loom Dad': Nostos in the 21st Century, in Odyssean Identities in Modern Cultures: the Journey Home, edited by Hunter Gardner and Sheila Murnaghan (Ohio State University Press, 2014), and “Mourning Lions and Penelope’s Revenge,” in the journal Arethusa (2016)."
            }, {
                'name': "David Elmer",
                'slug': "david-elmer",
                'avatarUrl': "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ%20/%20david_elmer.jpeg",
                'bio': "David F. Elmer is a Professor of the Classics at Harvard University. His recent publications include “The ‘Narrow Road’ and the Ethics of Language Use in the Iliad and Odyssey,” in volume 44 of Ramus (2015), and The Poetics of Consent: Collective Decision Making and the Iliad (Johns Hopkins UP, 2013)."
            }, {
                'name': "Douglas Frame",
                'slug': "douglas-frame",
                'avatarUrl': "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ%20/%20douglas_frame.png",
                'bio': "1971 Harvard Ph.D. in classical philology; until 2000 college teaching for some of the time, other pursuits for more of the time; since 2000 Associate Director of the Center for Hellenic Studies in Washington; after retiring as Associate Director in 2012 continued affiliation with the CHS."
            }, {
                'name': "Gregory Nagy",
                'slug': "gregory-nagy",
                'avatarUrl': "https://archimedes-data003.s3-us-west-2.amazonaws.com/FyHmZ3iW73t3CR2Jg/IMG_1040sel.jpg",
                'bio': "Born in Budapest, Hungary, Professor Nagy was educated at Indiana University and Harvard University, where he studied Classical Philology and Linguistics, receiving his Ph.D. in 1966. He has held positions at Johns Hopkins University and since 1975 in Harvard University, where he was named in 1984 the Francis Jones Professor of Classical Greek Literature and Professor of Comparative Literature. He is also currently the Curator of the Milman Parry Collection of Oral Literature and, since August of 2000, the Director of Harvard University's Center for Hellenic Studies. Professor Nagy has served as Chair of the Harvard University Classics Department and as President of the American Philological Association.<br><br>Professor Nagy is a renowned authority in the field of Homeric and related Greek studies. His numerous honors include a Guggenheim Fellowship and the Goodwin Award of Merit of the American Philological Association for his book, The Best of the Achaeans (1979). In addition to this path-breaking work, he has published Greek Dialects and the Transformation of an Indo-European Process (1970), Comparative Studies in Greek and Indic Meter (1974), Pindar’s Homer: The Lyric Possession of an Epic Past (1990), Greek Mythology and Poetics (1990), Poetry as Performance: Homer and Beyond (1996), Homeric Questions (1996) Plato's Rhapsody and Homer's Music (2002), Homeric Responses (2004), and Homer's Text and Language (2004); he has as well edited or co-edited various volumes and written almost a hundred articles and reviews (see his CV for a full list). Professor Nagy has lectured widely in North America and Europe on a great range of keywords, especially concentrated in Homeric and Archaic Greek questions. Some of his recent lecture texts are available on the CHS website here. He is a strong proponent of the use of technology in teaching, and in the teaching and use of student writing in the core curriculum."
            }, {
                'name': "Laura Slatkin",
                'slug': "laura-slatkin",
                'avatarUrl': "https://ahcip.chs.harvard.edu/images/default_user.jpg",
                'bio': "Laura M. Slatkin is a Professor of Classical Studies at New York University’s Gallatin School and Visiting Professor in the Committee on Social Thought, University of Chicago. She has taught at the University of California at Santa Cruz, Yale University, and Columbia University.  Her research and teaching interests include early Greek epic; wisdom traditions in classical and Near Eastern antiquity; and the reception of Homer. She has published articles on Greek epic and drama and most recently on 'British Romantic Homer';  The Power of Thetis and Selected Essays was published by the CHS in 2011.  She has served as the editor in chief of Classical Philology, and coedited Histories of Post-War French Thought, Volume 2: Antiquities (with G. Nagy and N. Loraux, New Press, 2001).  She is currently collaborating on a study of the reception of Homer in British romantic poetry."
            }, {
                'name': "Leonard Muellner",
                'slug': "leonard-muellner",
                'avatarUrl': "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ/leonard_muellner.jpg",
                'bio': "Leonard Muellner is Professor of Classical Studies Emeritus at Brandeis University and Director for IT and Publications at the Center for Hellenic Studies. Educated at Harvard (Ph.D. 1973), his scholarly interests center on Homeric epic, with special interests in historical linguistics, anthropological approaches to the study of myth, and the poetics of oral traditional poetry. His recent work includes 'Grieving Achilles,' in Homeric Contexts: Neoanalysis and the Interpretation of Oral Poetry, ed. A. Rengakos, F. Montanari, and C. Tsagalis, Trends in Classics, Supplementary Volume 12, Berlin, 2012, pp. 187-210, and “Homeric Anger Revisited,” Classics@ Issue 9: Defense Mechanisms, Center for Hellenic Studies, Washington, DC, September, 2011."
            }, {
                'name': "Mary Ebbott",
                'slug': "mary-ebbott",
                'avatarUrl': "https://ahcip.chs.harvard.edu/images/default_user.jpg",
                'bio': "Mary Ebbott is Associate Professor of Classics at the College of the Holy Cross in Worcester, Massachusetts. She is the author of Imagining Illegitimacy in Greek Literature (Lexington Books, 2003) and co-author, with Casey Dué, of Iliad 10 and the Poetics of Ambush (Center for Hellenic Studies, 2010)."
            }, {
                'name': "Olga Levaniouk",
                'slug': "olga-levaniouk",
                'avatarUrl': "https://ahcip.chs.harvard.edu/images/default_user.jpg",
                'bio': "At the center of my research is Homer as part of the song culture of Ancient Greece. My interests include myth, ritual, lyric poetry, drama, comparative and historical linguistics, oral traditional poetry and poetics in Greece and beyond, and a comparative approach to all of the above. I am particularly interested in comparative work involving Indic and Slavic poetry, and have also made forays into working with Turkic epic. I have at various points studied Sanskrit, Avestan, Hittite and Tocharian and retain various degrees of competency (and unflagging interest!) in these languages. On the myth side of things, I have a long-standing interest in mythological variation and local mythologies. On the poetic side, I have a particular interests in women's songs, especially laments and wedding songs.<br><br>My first book, Eve of the Festival: Making Myth in Odyssey 19, is a study of myth in Homer on the example of the first dialogue between Penelope and Odysseus.<br><br>My current project is a comparative study of Greek weddings focusing on the performances by the bride, her mother, and her age-mates."
            }, {
                'name': "Richard Martin",
                'slug': "richard-martin",
                'avatarUrl': "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ%20/%20richard_martin.jpg",
                'bio': "Richard P. Martin is the Antony and Isabelle Raubitschek Professor in Classics at Stanford University. He works mainly on Greek poetry (especially Homer and Aristophanes), religion, and mythology."
            }, {
                'name': "Thomas Walsh",
                'slug': "thomas-walsh",
                'avatarUrl': "https://ahcip.chs.harvard.edu/images/default_user.jpg",
                'bio': ""
            }, {
                'name': "Yiannis Petropoulos",
                'slug': "yiannis-petropoulos",
                'avatarUrl': "https://ahcip.chs.harvard.edu/images/default_user.jpg",
                'bio': ""
            }],
    },{
        'hostname': 'pindar',
        'commenters': [{
                'name': "Gregory Nagy",
                'slug': "gregory-nagy",
                'avatarUrl': "https://archimedes-data003.s3-us-west-2.amazonaws.com/FyHmZ3iW73t3CR2Jg/IMG_1040sel.jpg",
                'bio': "Born in Budapest, Hungary, Professor Nagy was educated at Indiana University and Harvard University, where he studied Classical Philology and Linguistics, receiving his Ph.D. in 1966. He has held positions at Johns Hopkins University and since 1975 in Harvard University, where he was named in 1984 the Francis Jones Professor of Classical Greek Literature and Professor of Comparative Literature. He is also currently the Curator of the Milman Parry Collection of Oral Literature and, since August of 2000, the Director of Harvard University's Center for Hellenic Studies. Professor Nagy has served as Chair of the Harvard University Classics Department and as President of the American Philological Association.<br><br>Professor Nagy is a renowned authority in the field of Homeric and related Greek studies. His numerous honors include a Guggenheim Fellowship and the Goodwin Award of Merit of the American Philological Association for his book, The Best of the Achaeans (1979). In addition to this path-breaking work, he has published Greek Dialects and the Transformation of an Indo-European Process (1970), Comparative Studies in Greek and Indic Meter (1974), Pindar’s Homer: The Lyric Possession of an Epic Past (1990), Greek Mythology and Poetics (1990), Poetry as Performance: Homer and Beyond (1996), Homeric Questions (1996) Plato's Rhapsody and Homer's Music (2002), Homeric Responses (2004), and Homer's Text and Language (2004); he has as well edited or co-edited various volumes and written almost a hundred articles and reviews (see his CV for a full list). Professor Nagy has lectured widely in North America and Europe on a great range of keywords, especially concentrated in Homeric and Archaic Greek questions. Some of his recent lecture texts are available on the CHS website here. He is a strong proponent of the use of technology in teaching, and in the teaching and use of student writing in the core curriculum."
            }, {
                'name': "Maša Culumovic",
                'slug': "masa-culumovic",
                'avatarUrl': "https://ahcip.s3-us-west-2.amazonaws.com/Culumovic.png",
                'bio': ""
            }],
    },{
        'hostname': 'pausanias',
        'commenters': [{
                'name': "Carolyn Higbie",
                'slug': "carolyn-higbie",
                'avatarUrl': "https://ahcip.chs.harvard.edu/images/default_user.jpg",
                'bio': ""
            }, {
                'name': "Gregory Nagy",
                'slug': "gregory-nagy",
                'avatarUrl': "https://archimedes-data003.s3-us-west-2.amazonaws.com/FyHmZ3iW73t3CR2Jg/IMG_1040sel.jpg",
                'bio': "Born in Budapest, Hungary, Professor Nagy was educated at Indiana University and Harvard University, where he studied Classical Philology and Linguistics, receiving his Ph.D. in 1966. He has held positions at Johns Hopkins University and since 1975 in Harvard University, where he was named in 1984 the Francis Jones Professor of Classical Greek Literature and Professor of Comparative Literature. He is also currently the Curator of the Milman Parry Collection of Oral Literature and, since August of 2000, the Director of Harvard University's Center for Hellenic Studies. Professor Nagy has served as Chair of the Harvard University Classics Department and as President of the American Philological Association.<br><br>Professor Nagy is a renowned authority in the field of Homeric and related Greek studies. His numerous honors include a Guggenheim Fellowship and the Goodwin Award of Merit of the American Philological Association for his book, The Best of the Achaeans (1979). In addition to this path-breaking work, he has published Greek Dialects and the Transformation of an Indo-European Process (1970), Comparative Studies in Greek and Indic Meter (1974), Pindar’s Homer: The Lyric Possession of an Epic Past (1990), Greek Mythology and Poetics (1990), Poetry as Performance: Homer and Beyond (1996), Homeric Questions (1996) Plato's Rhapsody and Homer's Music (2002), Homeric Responses (2004), and Homer's Text and Language (2004); he has as well edited or co-edited various volumes and written almost a hundred articles and reviews (see his CV for a full list). Professor Nagy has lectured widely in North America and Europe on a great range of keywords, especially concentrated in Homeric and Archaic Greek questions. Some of his recent lecture texts are available on the CHS website here. He is a strong proponent of the use of technology in teaching, and in the teaching and use of student writing in the core curriculum."
            }, {
                'name': "Greta Hawes",
                'slug': "greta-hawes",
                'avatarUrl': "https://ahcip.chs.harvard.edu/images/default_user.jpg",
                'bio': ""
            }, {
                'name': "Lia Hanhardt",
                'slug': "lia-hanhardt",
                'avatarUrl': "https://ahcip.chs.harvard.edu/images/default_user.jpg",
                'bio': ""
            }, {
                'name': "Scott Smith",
                'slug': "scott-smith",
                'avatarUrl': "https://ahcip.chs.harvard.edu/images/default_user.jpg",
                'bio': ""
            }],
    },{
        'hostname': 'herodotus',
        'commenters': [{
                'name': "Gregory Nagy",
                'slug': "gregory-nagy",
                'avatarUrl': "https://archimedes-data003.s3-us-west-2.amazonaws.com/FyHmZ3iW73t3CR2Jg/IMG_1040sel.jpg",
                'bio': "Born in Budapest, Hungary, Professor Nagy was educated at Indiana University and Harvard University, where he studied Classical Philology and Linguistics, receiving his Ph.D. in 1966. He has held positions at Johns Hopkins University and since 1975 in Harvard University, where he was named in 1984 the Francis Jones Professor of Classical Greek Literature and Professor of Comparative Literature. He is also currently the Curator of the Milman Parry Collection of Oral Literature and, since August of 2000, the Director of Harvard University's Center for Hellenic Studies. Professor Nagy has served as Chair of the Harvard University Classics Department and as President of the American Philological Association.<br><br>Professor Nagy is a renowned authority in the field of Homeric and related Greek studies. His numerous honors include a Guggenheim Fellowship and the Goodwin Award of Merit of the American Philological Association for his book, The Best of the Achaeans (1979). In addition to this path-breaking work, he has published Greek Dialects and the Transformation of an Indo-European Process (1970), Comparative Studies in Greek and Indic Meter (1974), Pindar’s Homer: The Lyric Possession of an Epic Past (1990), Greek Mythology and Poetics (1990), Poetry as Performance: Homer and Beyond (1996), Homeric Questions (1996) Plato's Rhapsody and Homer's Music (2002), Homeric Responses (2004), and Homer's Text and Language (2004); he has as well edited or co-edited various volumes and written almost a hundred articles and reviews (see his CV for a full list). Professor Nagy has lectured widely in North America and Europe on a great range of keywords, especially concentrated in Homeric and Archaic Greek questions. Some of his recent lecture texts are available on the CHS website here. He is a strong proponent of the use of technology in teaching, and in the teaching and use of student writing in the core curriculum."
            }, {
                'name': "Leonard Muellner",
                'slug': "leonard-muellner",
                'avatarUrl': "https://archimedes-data003.s3-us-west-2.amazonaws.com/JnSr8x5kfWvmeraLZ/leonard_muellner.jpg",
                'bio': "Leonard Muellner is Professor of Classical Studies Emeritus at Brandeis University and Director for IT and Publications at the Center for Hellenic Studies. Educated at Harvard (Ph.D. 1973), his scholarly interests center on Homeric epic, with special interests in historical linguistics, anthropological approaches to the study of myth, and the poetics of oral traditional poetry. His recent work includes 'Grieving Achilles,' in Homeric Contexts: Neoanalysis and the Interpretation of Oral Poetry, ed. A. Rengakos, F. Montanari, and C. Tsagalis, Trends in Classics, Supplementary Volume 12, Berlin, 2012, pp. 187-210, and “Homeric Anger Revisited,” Classics@ Issue 9: Defense Mechanisms, Center for Hellenic Studies, Washington, DC, September, 2011."
            }],
    },{
        'hostname': 'scs2020',
        'commenters': [{
                'name': "SCS Commenter",
                'avatarUrl': "https://ahcip.chs.harvard.edu/images/default_user.jpg",
                'bio': "Philologist and researcher at University X, creating commentaries for Society for Classical Studies",
            }],
    }]
