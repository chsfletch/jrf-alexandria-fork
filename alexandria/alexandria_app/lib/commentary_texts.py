COMMENTARY_TEXTS = [{
    'hostname': 'homer',
    'texts': [{
            'title': 'The Iliad',
            'byline': 'Homer',
            'url': '/read/urn:cts:greekLit:tlg0012.tlg001.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/achilles_2.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'The Odyssey',
            'byline': 'Homer',
            'url': '/read/urn:cts:greekLit:tlg0012.tlg002.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/odysseus.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 1 to Dionysus',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg001.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 2 to Demeter',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg002.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 3 to Delian and Pythian Apollo',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg003.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 4 to Hermes',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg004.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 5 to Aphrodite',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg005.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 6 to Aphrodite',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg006.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 7 to Dionysus',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg007.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 8 to Ares',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg008.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 9 to Artemis',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg009.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 10 to Aphrodite',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg010.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 11 To Athena',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg011.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 12 to Hera',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg012.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 13 to Demeter',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg013.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 14 to the Mother of the Gods',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg014.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 15 To Heracles',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg015.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 16 To Asclepius',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg016.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 17 To the Dioscuri',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg017.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 18 to Hermes',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg018.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 19 to Pan',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg019.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 20 To Hephaestus',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg020.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 21 To Apollo',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg021.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 22 To Poseidon',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg022.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 23 to Zeus',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg023.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 24 To Hestia',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg024.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 25 To the Muses and Apollo',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg025.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 26 To Dionysus',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg026.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 27 to Artemis',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg027.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 28 to Athena',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg028.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 29 to Hestia',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg029.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 30 to Earth',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg030.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 31 to Helios',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg031.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 32 To Selene',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg032.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }, {
            'title': 'Hymn 33 To the Dioscuri',
            'byline': 'Homeric Hymns',
            'url': '/read/urn:cts:greekLit:tlg0013.tlg033.perseus-grc2',
            'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
        }],
    }, {
        'hostname': 'pindar',
        'texts': [{
                'title': 'Olympian Odes',
                'byline': 'Pindar',
                'url': '/read/urn:cts:greekLit:tlg0033.tlg001.perseus-grc2',
                'thumbnailSrc': '//iiif.orphe.us/achilles_2.jpg/square/350,/0/default.jpg',
            }, {
                'title': 'Pythian Odes',
                'byline': 'Pindar',
                'url': '/read/urn:cts:greekLit:tlg0033.tlg002.perseus-grc2',
                'thumbnailSrc': '//iiif.orphe.us/odysseus.jpg/square/350,/0/default.jpg',
            }, {
                'title': 'Nemean Odes',
                'byline': 'Pindar',
                'url': '/read/urn:cts:greekLit:tlg0033.tlg003.perseus-grc2',
                'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
            }, {
                'title': 'Isthmean Odes',
                'byline': 'Pindar',
                'url': '/read/urn:cts:greekLit:tlg0033.tlg004.perseus-grc2',
                'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
            }],
    }, {
        'hostname': 'pausanias',
        'texts': [{
                'title': 'Description of Greece',
                'byline': 'Pausanias',
                'url': '/read/urn:cts:greekLit:tlg0525.tlg001.perseus-grc2',
                'thumbnailSrc': '//iiif.orphe.us/achilles_2.jpg/square/350,/0/default.jpg',
            }],
    }, {
        'hostname': 'herodotus',
        'texts': [{
                'title': 'Histories',
                'byline': 'Herodotus',
                'url': '/read/urn:cts:greekLit:tlg0016.tlg001.perseus-grc2',
                'thumbnailSrc': '//iiif.orphe.us/achilles_2.jpg/square/350,/0/default.jpg',
            }],
    }, {
        'hostname': 'scs2020',
        'texts': [{
                'title': 'The Iliad',
                'byline': 'Homer',
                'url': '/read/urn:cts:greekLit:tlg0012.tlg001.perseus-grc2',
                'thumbnailSrc': '//iiif.orphe.us/achilles_2.jpg/square/350,/0/default.jpg',
            }, {
                'title': 'The Argonautica',
                'byline': 'Apollonius Rhodius',
                'url': '/read/urn:cts:greekLit:tlg0548.tlg001.perseus-grc2',
                'thumbnailSrc': '//iiif.orphe.us/odysseus.jpg/square/350,/0/default.jpg',
            }, {
                'title': 'Eumenides',
                'byline': 'Aeschylus',
                'url': '/read/urn:cts:greekLit:tlg0085.tlg007.perseus-grc1',
                'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
            }, {
                'title': 'The Aeneid',
                'byline': 'Vergil',
                'url': '/read/urn:cts:latinLit:phi0690.phi003.perseus-lat1',
                'thumbnailSrc': '//iiif.orphe.us/sirens.jpg/square/350,/0/default.jpg',
            }],
    }]
