import copy
from operator import attrgetter
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.cache import cache
from django.urls import reverse
from django.core.paginator import Paginator

from ..models import Project, Comment, TextMeta
from .parse_urn import parse_urn, urn_has_edition
from .textserver import Textserver
from .default_text_images import DEFAULT_TEXT_IMAGES
from .comment_utils import get_comments_from_urn, get_commentaries_from_urn
from .utils import pick_a_text, get_params, reverse_querystring

from time import time
from pprint import pprint


def get_project_commenters(comment_list):
    """get distinct commenters from comments of this project

    Arguments:
        project {[type]} -- [description]
    """
    project_commenters = []
    for comment in comment_list:
        for commenter in comment.commenters.all():
            project_commenters.append(commenter)
    project_commenters = set(project_commenters)
    project_commenters = sorted(project_commenters, key=attrgetter('last_name'))
    return project_commenters


def parse_read_urn(unique_urns):
    for unique_urn in unique_urns:
        read_urn = parse_urn(unique_urn["urn"], "work")
        if read_urn and urn_has_edition(unique_urn["urn"]):
            return read_urn


def populate_collection_textgroup(text_data):
    collections_data = text_data['collections']
    text_groups_data = text_data['textGroups']

    relevant_works = []
    if 'workSearch' in text_data:
        relevant_works = text_data['workSearch']['works']
    else:
        relevant_works = text_data['works']

    # generate a map of textgroup and collections
    text_groups_map = {}
    for text_group in text_groups_data:
        text_groups_map[text_group['id']] = text_group
    collections_map = {}
    for collection in collections_data:
        collections_map[collection['id']] = collection

    for work in relevant_works:
        if work['textGroupID'] in text_groups_map:
            work['text_group_title'] = text_groups_map[work['textGroupID']]['title']
            if text_groups_map[work['textGroupID']]['collectionId'] in collections_map:
                work['collection_title'] = collections_map[text_groups_map[work['textGroupID']]['collectionId']]['title']

    if 'workSearch' in text_data:
        text_data['workSearch']['works'] = relevant_works
    else:
        text_data['works'] = relevant_works

    return text_data

def parse_text_data(text_data, text_data_unfiltered = None):
    # get relevant works from textserver works
    relevant_works = []
    all_works = []
    if not text_data_unfiltered:
        text_data_unfiltered = text_data
    if 'workSearch' in text_data:
        relevant_works = text_data['workSearch']['works']
        all_works = text_data_unfiltered['workSearch']['works']
    else:
        relevant_works = text_data['works']
        all_works = text_data_unfiltered['works']

    # init collection and textgroup data dicts
    collections_data = text_data['collections']
    text_groups_data = text_data['textGroups']
    languages_data = text_data['languages']



    # generate text meta mapper for O(1) cover image getter
    textmeta_all = TextMeta.objects.all()
    urn_cover_image_map = {}
    for tm in textmeta_all:
        urn_cover_image_map[tm.urn] = tm.cover_image.url

    # parse works into textgroups
    text_groups = {}
    for work in relevant_works:
        if work['full_urn']:
            read_urn = work['full_urn']

        # generate static image urls for default images
        cover_image = static(f"img/{DEFAULT_TEXT_IMAGES[0]}")
        if work['urn'] in urn_cover_image_map:
            cover_image = urn_cover_image_map[work['urn']]
        work['image_url'] = cover_image

        # textgroup as a work card
        if work['urn'] in text_groups:
            text_groups[work['urn']]['works'].append(work)
        else:
            text_groups[work['urn']] = {
                'english_title': work['english_title'],
                'text_group_urn': work['urn'],
                'editions_count': 0,
                'translations_count': 0,
                'image_url': cover_image,
                'works': [work],
            }

        # increment Edition count for list display
        if work['work_type'] == 'edition':
            text_groups[work['urn']]['editions_count'] += 1;
        # increment Translation count for list display
        elif work['work_type'] == 'translation':
            text_groups[work['urn']]['translations_count'] += 1;

        # determine if it's main work
        main_work = pick_a_text([work])
        if main_work and main_work['language']:
            # get language field for list display
            text_groups[work['urn']]['language'] = main_work['language']['title']
            text_groups[work['urn']]['default_text'] = main_work

    # select main work
    text_groups_list = list(text_groups.values())
    text_groups_list_with_default_work = []
    for textgroup in text_groups_list:
        main_work = pick_a_text(textgroup['works'])
        textgroup['default_text'] = main_work
        # get language field for list display
        textgroup['language'] = main_work['language']['title']
        if main_work:
            text_groups_list_with_default_work.append(textgroup)

    result = {
        'works': text_groups_list_with_default_work,
        'collections': collections_data,
        'text_groups': text_groups_data,
        'languages': languages_data,
    }
    return result

def filter_works(text_data, request):

    # init
    works = []
    if 'workSearch' in text_data:
        works = text_data['workSearch']['works']
    else:
        works = text_data['works']

    work_ids = set()
    for work in works:
        work_ids.add(work['id'])

    ## filters
    # language filter
    language = request.GET.get('Language')
    if language:
        language_work_ids = set()
        for work in works:
            if work['language'] and work['language']['title'] == language:
                language_work_ids.add(work['id'])
        work_ids = work_ids.intersection(language_work_ids)


    # work type filter
    work_type = request.GET.get('Work Type')
    if work_type:
        work_type_work_ids = set()
        for work in works:
            if work['work_type'] == work_type:
                work_type_work_ids.add(work['id'])
        work_ids = work_ids.intersection(work_type_work_ids)

    # structure filter
    structure = request.GET.get('Structure')
    if structure:
        structure_work_ids = set()
        for work in works:
            if work['structure'] == structure:
                structure_work_ids.add(work['id'])
        work_ids = work_ids.intersection(structure_work_ids)

    # collection filter
    collection = request.GET.get('Collection')
    if collection:
        collection_work_ids = set()
        for work in works:
            if 'collection_title' in work and work['collection_title'] == collection:
                collection_work_ids.add(work['id'])
        work_ids = work_ids.intersection(collection_work_ids)

    # textgroup filter
    text_group = request.GET.get('Author')
    if text_group:
        text_group_work_ids = set()
        for work in works:
            if work['text_group_title'] == text_group:
                text_group_work_ids.add(work['id'])
        work_ids = work_ids.intersection(text_group_work_ids)

    works_filtered = []
    for work in works:
        if work['id'] in work_ids:
            works_filtered.append(work)

    if 'workSearch' in text_data:
        text_data['workSearch']['works'] = works_filtered
    else:
        text_data['works'] = works_filtered

    return text_data

def get_relevant_works(project, request=None):

    # get unique urns from comments
    unique_urns = Comment.objects.filter(project_id=project.id).values('urn').distinct()
    unique_urn_set = set()
    for urn in unique_urns:
        urn_parsed = parse_urn(urn['urn'], "text_group")
        if urn_parsed:
            unique_urn_set.add(urn_parsed)

    # for `read the commentary` link urn
    read_urn = parse_read_urn(unique_urns)

    # get works from unique urns with comments
    text_fetcher = Textserver()
    text_data = text_fetcher.fetch_works_by_urn_list(list(unique_urn_set))

    # populate collection and textgroup title
    text_data = populate_collection_textgroup(text_data)
    text_data_unfiltered = copy.deepcopy(text_data)

    # filter works
    if request:
        text_data = filter_works(text_data, request)

    # parse collection/textgroup
    text_data_parsed = parse_text_data(text_data, text_data_unfiltered)

    return (text_data_parsed, read_urn)


def _fetch_all_works():
    # get works from unique urns with comments
    text_data = cache.get('all_works')

    if not text_data:
        text_fetcher = Textserver()
        text_data = text_fetcher.fetch_all_works()
        cache.set('all_works', text_data, 60 * 60 * 15)

    return text_data


def _search_works(search_term):
    text_fetcher = Textserver()
    cache_key = f'works_{search_term}'
    text_data = cache.get(cache_key)
    if not text_data:
        text_data = text_fetcher.search_works(search_term)
        cache.set(cache_key, text_data, 60 * 60 * 15)
    else:
        pass
    return text_data


def get_all_works(request=None):
    t0 = time()
    # get params from request
    params_named = get_params(request)
    page = 1

    if 'page' in params_named:
        page = int(params_named['page'])

    text_data = None

    if 'search' in params_named:
        text_data = _search_works(params_named['search'])
    else:
        text_data = _fetch_all_works()

    print(f"get_all_works: _fetch_all_works() - {time()-t0}")

    # filter works
    text_data_unfiltered = None

    if request:
        text_data_unfiltered = copy.deepcopy(text_data)

        # populate collection and textgroup title
        text_data = populate_collection_textgroup(text_data)

        text_data = filter_works(text_data, request)

    text_data_parsed = parse_text_data(text_data, text_data_unfiltered)

    print(f"get_all_works: filter works - {time()-t0}")

    # pagination
    offset = 0
    page_size = 10

    if page > 0:
        offset = (page - 1) * page_size

    start_index = offset
    end_index = offset + page_size
    paginator = Paginator(text_data_parsed['works'], page_size)
    pagination = paginator.page(page)
    text_data_parsed['works'] = pagination.object_list

    print(f"get_all_works: pagination - {time()-t0}")

    return text_data_parsed, pagination


def fetch_text_meta(texts):
    for text in texts:
        try:
            text_meta = TextMeta.objects.get(urn=text['full_urn'])
            text['text_meta'] = text_meta
        except:
            text['text_meta'] = {}
    return texts
