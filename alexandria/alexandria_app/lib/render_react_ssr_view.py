"""
Render a react view. Based on
https://github.com/dhmit/rereading/blob/master/backend/apps/common.py
"""
from django.conf import settings
from django.shortcuts import render
from requests import post

from ..models import Project
from ..lib.get_archive_hostname import get_archive_hostname

def user_is_admin(user, hostname):
    try:
        return len(Project.objects.filter(
            admins=user, hostname=hostname))
    except:
        return 0


def user_is_editor(user, hostname):
    try:
        return len(Project.objects.filter(
            editors=user, hostname=hostname))
    except:
        return 0


def user_is_commenter(user, hostname):
    try:
        return len(Project.objects.filter(
            commenters=user, hostname=hostname))
    except:
        return 0


def set_user_permissions(request, context={}):
    if request.user.is_authenticated:
        user = request.user
        hostname = get_archive_hostname(request)
        context['USER_IS_ADMIN'] = user_is_admin(user, hostname)
        context['USER_IS_EDITOR'] = user_is_editor(user, hostname)
        context['USER_IS_COMMENTER'] = user_is_commenter(user, hostname)
        context['USER_IS_LOGGED_IN'] = 1
    else:
        context['USER_IS_ADMIN'] = 0
        context['USER_IS_EDITOR'] = 0
        context['USER_IS_COMMENTER'] = 0
        context['USER_IS_LOGGED_IN'] = 0

    return context


def set_ssr_content(request, context):
    req = post("{}/read/{}".format(
        settings.RENDERING_SERVER_URL,
        context['urn']), json=context, headers={'Host': request.get_host()})

    resp_json = req.json()
    context['REACT_APP'] = resp_json.get('html')
    context['SSR_CSS'] = resp_json.get('css')

    return context

def render_react_ssr_view(request, template="react_ssr_view.twig", context={}):
    """
    A view function to render views that are entirely managed
    in the frontend by a single React component. This lets us use
    Django url routing with React components.

    :param request: Django request object to pass through
    :param template: name of template to render
    :param context: Django context

    :return:
    """

    context = set_user_permissions(request, context)
    context = set_ssr_content(request, context)

    return render(request, template, context)
