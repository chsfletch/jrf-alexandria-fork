import rules

from django.shortcuts import get_object_or_404

from ..models import Project

# define rule checkers
@rules.predicate
def is_project_admin(user, hostname):
    project = get_object_or_404(Project, hostname=hostname)
    return user in project.admins.all()

# add rules
rules.add_rule('can_access_commentary_dashboard', is_project_admin)