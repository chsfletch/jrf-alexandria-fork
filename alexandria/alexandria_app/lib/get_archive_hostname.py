"""
Get the full domain of the current request to the server from the client for determining tenancy
"""


def get_archive_hostname(request, from_origin=False):
    """
    :param request: Django request object with http host

    :return string: The hostname to determine tenancy
    """
    hostname = request.META['HTTP_HOST']
    if from_origin and request.META.get('HTTP_ORIGIN'):
        hostname = request.META['HTTP_ORIGIN'].split('://')[1]
    alexandria_hostnames = [
        'alex-django.herokuapp.com',
        'staging.alexandria.archimedes.digital',
        'alexandria.archimedes.digital',
        'staging.alexandria.info',
        'staging.alexandria.local',
        'alexandria.info',
        'alexandria.local',
        'localhost'
    ]
    # remove port
    if ":" in hostname:
        hostname = hostname.split(":")[0]

    # if in list of alexandria non-tenant domain names, return empty string
    if hostname in alexandria_hostnames:
        hostname = ''

        # regularlize development domain
    if hostname.endswith('alexandria.local'):
        hostname = hostname.replace('alexandria.local', 'alexandria.info')

        # beta subdomain for testing production tenants
    hostname = hostname.replace('.beta', '')
    hostname = hostname.replace('beta', '')
    hostname = hostname.replace('.staging', '')

    # individual project domain mapping (until better system for managing this is added)
    hostname = hostname.replace('homer.chs.harvard.edu', 'homer.newalexandria.info')
    hostname = hostname.replace('homer.chs.harvard.local', 'homer.newalexandria.info')

    # right now just subdomain is stored in hostname fields for existing commentaries,
    # figure out a way to track entire hostname in the future
    hostname = hostname.replace('.alexandria.info', '')
    hostname = hostname.replace('.newalexandria.info', '')

    # temporary archimedes digital domain replacements
    hostname = hostname.replace('.alexandria.archimedes.digital', '')
    hostname = hostname.replace('.staging.alexandria.archimedes.digital', '')
    hostname = hostname.replace('.alex-django.herokuapp.com', '')

    return hostname
