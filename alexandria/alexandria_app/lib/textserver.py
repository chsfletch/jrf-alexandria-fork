"""[summary]

Returns:
    [type] -- [description]
"""
import json
from django.conf import settings
from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport


class Textserver(object):
    def __init__(self):
        self.client = Client(
            transport=RequestsHTTPTransport(
                url=settings.TEXTSERVER or 'http://text.chs.harvard.edu/graphql')
        )

    def fetch_textnodes(self, urn):
        query = gql('''
            {
                textNodes(
                    urn:"%s"
                    pageSize: 1000
                ) {
                    id
                    text
                    urn
                }
            }
        ''' % urn)
        return self.client.execute(query)

    def search_works(self, search_term):
        query = gql('''
            {
                workSearch(
                    textsearch: "%s"
                    limit: 0
                ) {
                    total
                    works {
                        id
                        english_title
                        original_title
                        description
                        structure
                        form
                        urn
                        full_urn
                        work_type
                        label
                        language {
                            id
                            title
                        }
                        textGroupID
                    }
                }
                languages(
                    limit: 65535
                ) {
                    id
                    title
                    slug
                }
                collections(
                    limit: 65535
                ) {
                    id
                    title
                }
                textGroups(
                    limit: 65535
                ) {
                    id
                    title
                    collectionId
                }
            }
        ''' % search_term)
        return self.client.execute(query)

    def fetch_works_by_urn_list(self, urn_list):
        query = gql('''
            {
                workSearch(
                    urns: ["%s"]
                    limit: 0
                ) {
                    total
                    works {
                        id
                        english_title
                        original_title
                        description
                        structure
                        form
                        urn
                        full_urn
                        work_type
                        label
                        language {
                            id
                            title
                        }
                        textGroupID
                    }
                }
                languages(
                    limit: 65535
                ) {
                    id
                    title
                    slug
                }
                collections(
                    limit: 65535
                ) {
                    id
                    title
                }
                textGroups(
                    limit: 65535
                ) {
                    id
                    title
                    collectionId
                }
            }
        ''' % "\",\"".join(urn_list))

        return self.client.execute(query)

    def fetch_works_by_urn(self, full_urn):
        query = gql('''
            {
                workByUrn(
                    full_urn: "%s"
                ) {
                    id
                    slug
                    english_title
                    original_title
                    form
                    work_type
                    label
                    description
                    urn
                    full_urn
                    language {
                        id
                        title
                        slug
                    }
                    version {
                        id
                        slug
                        title
                        description
                    }
                    exemplar {
                        id
                        slug
                        title
                        description
                    }
                    refsDecls {
                        description
                        label
                    }
                    translation {
                        id
                        slug
                        title
                        description
                    }
                    textNodes {
                        id
                        index
                        location
                        text
                        urn
                    }
                }
            }
        ''' % full_urn)
        return self.client.execute(query)

    def fetch_work_by_id(self, work_id):
        query = gql('''
            {
                work(
                    id: %s
                ) {
                    id
                    english_title
                    original_title
                    language {
                        id
                        title
                    }
                    urn
                    full_urn
                }
            }
        ''' % work_id)
        return self.client.execute(query)

    def fetch_textgroups(self, urn):
        query = gql('''
            {
                textGroups(
                    urn:"%s"
                ) {
                    id
                    title
                    urn
                }
            }
        ''' % urn)
        return self.client.execute(query)

    def fetch_works(self, urn):
        query = gql('''
            {
                works(
                    urn:"%s"
                ) {
                    id
                    english_title
                    original_title
                    urn
                    full_urn
                    work_type
                    description
                    language {
                        id
                        title
                        slug
                    }
                }
            }
        ''' % urn)
        return self.client.execute(query)

    def fetch_all_works(self):
        query = gql('''
            {
                works (
                    limit: 0
                ) {
                    id
                    english_title
                    original_title
                    description
                    structure
                    form
                    urn
                    full_urn
                    work_type
                    label
                    language {
                        id
                        title
                        slug
                    }
                    textGroupID
                }
                languages(
                    limit: 65535
                ) {
                    id
                    title
                    slug
                }
                collections(
                    limit: 65535
                ) {
                    id
                    title
                }
                textGroups(
                    limit: 65535
                ) {
                    id
                    title
                    collectionId
                }
            }
        ''')
        return self.client.execute(query)

    def fetch_search_facets(self):
        query = gql('''
            query fetchSearchFacets {
              collections {
                id
                title
                slug
                urn
                textGroups {
                  id
                  title
                  slug
                  urn
                }
              }
              languages {
                id
                title
                slug
              }
            }
        ''')
        return self.client.execute(query)
