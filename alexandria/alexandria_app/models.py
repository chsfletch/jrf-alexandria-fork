from django.contrib.auth.models import AbstractUser
from django.db import models
from django.contrib.postgres.fields import JSONField

from tinymce.models import HTMLField

from .lib.utils import generate_comment_citation_urn


PUBLIC = 'PUBLIC'
PRIVATE = 'PRIVATE'
PRIVACY_CHOICES = [
    (PUBLIC, 'Public'),
    (PRIVATE, 'Private'),
]

EDITION = 'EDITION'
TRANSLATION = 'TRANSLATION'
CONTENT_TYPE_CHOICES = [
    (EDITION, 'Edition'),
    (TRANSLATION, 'Translation'),
]


class CustomUser(AbstractUser):
    full_name = models.CharField(max_length=256, blank=True)
    bio = models.TextField(blank=True)
    tagline = models.TextField(blank=True)
    picture = models.ImageField(
        upload_to='profile_images', blank=True)

    def __str__(self):
        return self.username


class Project(models.Model):
    hostname = models.CharField(max_length=256, unique=True)
    title = models.CharField(max_length=512)
    subtitle = models.TextField(blank=True)
    homepage_quote = models.CharField(max_length=1024, blank=True)
    homepage_quote_byline = models.CharField(max_length=1024, blank=True)
    privacy = models.CharField(
        max_length=8,
        choices=PRIVACY_CHOICES,
        default=PRIVATE,
    )
    canonical_domain = models.CharField(max_length=1024, blank=True)
    background_image = models.ImageField(
        upload_to='project_images', blank=True)
    admins = models.ManyToManyField(
        'CustomUser', related_name='admins', blank=True)
    editors = models.ManyToManyField(
        'CustomUser', related_name='editors', blank=True)
    commenters = models.ManyToManyField(
        'CustomUser', related_name='commenters', blank=True)
    about_html = HTMLField(blank=True)
    email = models.CharField(max_length=512, blank=True)
    address = models.CharField(max_length=512, blank=True)
    external_url = models.CharField(max_length=512, blank=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.hostname}'


class Bookmark(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    urn = models.CharField(max_length=1024, unique=True)
    title = models.CharField(max_length=256, null=True)

    def __str__(self):
        return self.urn


class Tag(models.Model):
    name = models.CharField(max_length=256)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    comments = models.ManyToManyField(
        'Comment', related_name='comments', blank=True)
    description = HTMLField(blank=True)
    image = models.ImageField(
        upload_to='project_images', blank=True)

    class Meta:
        unique_together = ('name', 'project',)

    def __str__(self):
        return self.name


class Comment(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    urn = models.CharField(max_length=1024)
    comment_citation_urn = models.CharField(max_length=1024, blank=True)
    privacy = models.CharField(
        max_length=8,
        choices=PRIVACY_CHOICES,
        default=PUBLIC,
    )
    # related_name as 'authors' to avoid clash of many to many association name space with project.commenters
    commenters = models.ManyToManyField('CustomUser', related_name='authors')
    tags = models.ManyToManyField('Tag', related_name='tags', blank=True)
    featured = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        force_generate_comment_citation_urn = kwargs.get(
            'force_generate_comment_citation_urn')
        # generate comment_citation_urn on save()
        if not self.comment_citation_urn or force_generate_comment_citation_urn:
            self.comment_citation_urn = generate_comment_citation_urn(self)
        super(Comment, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.id}: {self.urn}'

class RevisionBase(models.Model):
    """A revision of the content of a Comment
    A list of revisions ordered by creation datetime composes the Revisions of a Comment

    Arguments:
        models {[type]} -- [description]

    Returns:
        [type] -- [description]
    """
    text = models.TextField()
    text_raw = models.TextField(blank=True, null=True)
    title = models.CharField(max_length=256, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return self.text

class UserText(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    urn = models.CharField(max_length=1024)
    content_type = models.CharField(
        max_length=16,
        choices=CONTENT_TYPE_CHOICES,
    )
    privacy = models.CharField(
        max_length=8,
        choices=PRIVACY_CHOICES,
        default=PUBLIC,
    )
    users = models.ManyToManyField('CustomUser')
    selected_revision = models.ForeignKey('RevisionUserText',
        on_delete=models.CASCADE,
        null=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.urn

class RevisionComment(RevisionBase):
    comment = models.ForeignKey(
        Comment, related_name='revisions', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Comment Revision"
        verbose_name_plural = "Comment Revisions"


class RevisionUserText(RevisionBase):
    user_text = models.ForeignKey(
        UserText, related_name='revisions', on_delete=models.CASCADE)


class TextMeta(models.Model):
    """urn specifies which text (textserver.work) this meta describes

    Arguments:
        models {[type]} -- [description]

    Returns:
        [type] -- [description]
    """
    urn = models.CharField(max_length=1024)
    creator = models.CharField(max_length=256, blank=True)
    date = models.CharField(max_length=256, blank=True)
    cover_image = models.ImageField(
        upload_to='text_images', blank=True)
    description = HTMLField(blank=True)
    location = JSONField(blank=True, null=True)

    class Meta:
        pass

    def __str__(self):
        return self.urn
