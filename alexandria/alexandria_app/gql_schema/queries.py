import graphene
from django.core.paginator import Paginator

from alexandria_app.models import Project, Comment, UserText

from .type.project import ProjectType
from .type.comment import CommentType
from .type.revision_comment import RevisionCommentType
from .type.author import CustomUserType
from .type.tag import TagType
from .type.user_text import UserTextType
from .type.revision_user_text import RevisionUserTextType


class Privacy(graphene.Enum):
    PUBLIC = 'Public'
    PRIVATE = 'Private'


class Query(object):
    projects = graphene.List(ProjectType)
    project = graphene.Field(ProjectType, hostname=graphene.String())

    user_texts = graphene.List(UserTextType,
                               privacy=Privacy(),
                               search_term=graphene.String(),
                               page_size=graphene.Int(default_value=10),
                               page_number=graphene.Int(default_value=0)
                               )

    def resolve_projects(self, info, **kwargs):
        return Project.objects.all()

    def resolve_project(self, info, hostname):
        return Project.objects.get(hostname=hostname)

    def resolve_user_texts(self, info, **kwargs):
        search_term = kwargs.get('search_term')
        user_text_list = []
        if search_term:
            user_text_list = UserText.objects.filter(
                revisionusertext__text__contains=search_term)
        else:
            user_text_list = UserText.objects.all()
        paginator = Paginator(user_text_list, kwargs.get('page_size'))
        page_obj = paginator.get_page(kwargs.get('page_number'))
        return page_obj
