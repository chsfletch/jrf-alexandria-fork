from graphene_django.types import DjangoObjectType

from alexandria_app.models import UserText

class UserTextType(DjangoObjectType):
    class Meta:
        model = UserText