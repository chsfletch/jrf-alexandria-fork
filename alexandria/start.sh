#!/bin/bash

python --version
pip --version

# run db migrations
python manage.py migrate

# start ssr and gunicorn
pm2 start ./client/build/ssr/server/index.js --output ssr.out --error ssr.error
gunicorn -b :8000 --log-level debug --timeout 300 -w 8 config.wsgi
