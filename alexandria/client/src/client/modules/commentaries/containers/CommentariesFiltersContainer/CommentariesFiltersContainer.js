import React from 'react';
import PropTypes from 'prop-types';

import Filters from '../../../../components/common/Filters';

class CommentariesFiltersContainer extends React.Component {
	constructor(props) {
		super(props);
	}

	render () {
		let filters = this.props.filters || [];
		let relevantFilters = this.props.relevantFilters || [];

		return (
			<div>
				{this.props.loading ? (
					<Filters
						filters={filters}
						loading
					/>
				) : (
					<Filters
						filters={filters}
						relevantFilters={relevantFilters}
					/>
				)}
			</div>
		);
	}
}

CommentariesFiltersContainer.propTypes = {
	filters: PropTypes.array,
	loading: PropTypes.bool,
	relevantFilters: PropTypes.array,
}

export default CommentariesFiltersContainer;
