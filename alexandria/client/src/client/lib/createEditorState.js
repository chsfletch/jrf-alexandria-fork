/**
 * @flow
 * @prettier
 */

import * as React from 'react';
import {
	ContentState,
	EditorState,
	convertFromHTML,
	convertFromRaw,
} from 'draft-js';

import decorators from '#common/modules/orpheus-editor/components/decorators';

export default function createEditorState(text: string): EditorState {
	if (!text) return null;

	let baseContent = null;

	try {
		baseContent = convertFromRaw(JSON.parse(text));
	} catch (e) {
		const { contentBlocks, entityMap } = convertFromHTML(text || '');
		baseContent = ContentState.createFromBlockArray(contentBlocks, entityMap);
	}

	return EditorState.createWithContent(baseContent, decorators);
}
