import React from 'react';
import { shallow } from 'enzyme';

// component
import Text from './Text';

describe('Text', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<Text to="/"/>);
		expect(wrapper).toBeDefined();
	});
});
