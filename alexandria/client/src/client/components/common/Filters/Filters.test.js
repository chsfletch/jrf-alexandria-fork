import React from 'react';
import { shallow } from 'enzyme';

// component
import Filters from './Filters';

describe('Filters', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<Filters to="/"/>);
		expect(wrapper).toBeDefined();
	});
});
