import React from 'react';
import { shallow } from 'enzyme';

// component
import ListLoadingItem from './ListLoadingItem';

describe('ListLoadingItem', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<ListLoadingItem />);
		expect(wrapper).toBeDefined();
	});
});
