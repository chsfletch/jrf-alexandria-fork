/**
 * @prettier
 */

import { AtomicBlockUtils, EditorState, Modifier } from 'draft-js';
import getCurrentBlock from './getCurrentBlock';

export default (
	editorState,
	{ entityType = 'ITEM', entityMutability = 'IMMUTABLE', entityData = {} }
) => {
	let newContentState = editorState
		.getCurrentContent()
		.createEntity(entityType, entityMutability, entityData);

	newContentState = Modifier.replaceText(
		newContentState,
		editorState.getSelection().merge({
			anchorOffset: 0,
			focusOffset: getCurrentBlock(editorState).getCharacterList().size,
		}),
		''
	);

	const newEditorState = EditorState.push(
		editorState,
		newContentState,
		'apply-entity'
	);
	const entityKey = newContentState.getLastCreatedEntityKey();

	return AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, ' ');
};
