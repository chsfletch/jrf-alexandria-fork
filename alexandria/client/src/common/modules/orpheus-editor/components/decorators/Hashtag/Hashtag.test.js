import React from 'react';
import { convertFromRaw, genKey } from 'draft-js';
import { shallow } from 'enzyme';

import { Hashtag } from './Hashtag';

describe('Hashtag', () => {
  const rawContent = {
    blocks: [
      {
        entityRanges: [{ key: 'hashtag', length: 10, offset: 0 }],
        key: genKey(),
        text: ' ',
        type: 'atomic',
      },
    ],
    entityMap: {
      hashtag: {
        data: {
          label: 'test-hashtag-text',
        },
        mutability: 'IMMUTABLE',
        type: 'HASHTAG',
      },
    },
  };

	it('renders correctly', () => {
    const contentState = convertFromRaw(rawContent);
    const entityKey = contentState.getLastCreatedEntityKey();

		const wrapper = shallow(
      <Hashtag contentState={contentState} entityKey={entityKey}>
        <p>Testing hashtags</p>
      </Hashtag>
		);
		expect(wrapper).toBeDefined();
	});
});
