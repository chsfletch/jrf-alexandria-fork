import * as React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

import App from './components/App';
import Root from '#common/containers/Root';

const Reading = ({ project }) => {
	return (
		<Root>
			<Route path="/:urn" render={({ match: { params: { urn } } }) => {
				return <App project={project} urn={urn} />
			}} />
		</Root>
	);
};

Reading.propTypes = {
	project: PropTypes.object,
};

export default Reading;
