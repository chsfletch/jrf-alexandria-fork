/**
 * @prettier
 */

import gql from 'graphql-tag';

const projectCommentsQuery = gql`
	query projectCommentsQuery($hostname: String, $urn: String!, $limit: Int) {
		project(hostname: $hostname) {
			_id
			commentsOn(urn: $urn, limit: $limit) {
				_id
				status
				tenantId

				urn {
					v1
					v2
				}

				originalDate
				updated

				latestRevision {
					_id
					title
					text
					textRaw
					slug
					originalDate
					tenantId
					created
				}

				lemmaCitation {
					ctsNamespace
					textGroup
					work
					edition
					passageFrom
					passageTo
					subreferenceIndexFrom
					subreferenceIndexTo
				}
			}
		}
	}
`;

export default projectCommentsQuery;
