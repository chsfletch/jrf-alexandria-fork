/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';

import App from '../components/App.js';

const ReadingContainer = ({ urn }) => {
	return <App urn={urn} />;
};

ReadingContainer.propTypes = {
	urn: PropTypes.string.isRequired,
};

export default ReadingContainer;
