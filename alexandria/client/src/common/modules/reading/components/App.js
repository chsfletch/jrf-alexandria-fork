/**
 * @prettier
 */

import React, { useEffect, useReducer, useState } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { useQuery } from '@apollo/client';

import Header from './ReadingEnv/Header';
import Reader from './ReadingEnv/Reader';
import RevisionsModal from './ReadingEnv/RevisionsModal';
import SidePanel from './ReadingEnv/SidePanel';
import TextNode from './ReadingEnv/TextNode';
import NotFound from '#common/components/common/NotFound';

import APP_INIT_QUERY, { USER_GENERATED_CONTENT_QUERY } from './App.graphql';

import { parseValueUrn as parseUrn } from '../lib/parseUrn';
import canEditComments from '../lib/canEditComments';
import getCitationFromURN from '../lib/getCitationFromURN';
import getCurrentLocationSpan from '../lib/getCurrentLocationSpan';
import getHeadings from '../lib/getHeadings';
import updateUrl from '../lib/updateUrlWithPassageRange';

import {
	ReadingEnvDispatch,
	SET_CURRENT_COMMENT,
	SET_REVISIONS_MODAL_STATE,
	SET_SIDE_PANEL_STATE,
} from '../constants';

const textGroupUrn = (urn = '') => (urn.split('.') || [])[0];
const withoutPassageCitation = (urn = '') =>
	urn
		.split(':')
		.slice(0, 4)
		.join(':');

const appReducer = (state, action = { type: null }) => {
	switch (action.type) {
		case SET_CURRENT_COMMENT:
			return {
				...state,
				commentFormOpen: true,
				currentComment: action.currentComment,
			};
		case SET_REVISIONS_MODAL_STATE:
			return {
				...state,
				revisionsModalCurrentRevisable: action.revisionsModalCurrentRevisable,
				revisionsModalOpen: action.revisionsModalOpen,
			};
		case SET_SIDE_PANEL_STATE:
			return {
				...state,
				commentFormOpen: action.commentFormOpen,
				currentComment: action.currentComment,
				currentTranslation: action.currentTranslation,
				currentUrn: action.currentUrn,
				selectionText: action.selectionText || '',
				selectionUrn: action.selectionUrn,
				sidePanelOpen: action.sidePanelOpen,
				translationFormOpen: action.translationFormOpen,
			};
		default:
			throw new Error(`Invalid action type: ${action.type}.`);
	}
};

const initialState = {
	commentFormOpen: false,
	currentComment: {},
	currentTranslation: {},
	language: '',
	revisionsModalCurrentRevisable: null,
	revisionsModalOpen: false,
	selectionText: '',
	selectionUrn: '',
	sidePanelOpen: false,
	translationFormOpen: false,
};

const App = ({ project, urn }) => {
	const [state, dispatch] = useReducer(appReducer, initialState);
	const { passage } = parseUrn(urn);
	const urnWithoutPassageCitation = withoutPassageCitation(urn);
	let startsAtLocation = null;
	let endsAtLocation = null;

	if (passage && passage.length) {
		startsAtLocation = passage[0];
		if (passage.length > 1) {
			endsAtLocation = passage[1];
		}
	}

	const { loading, error, data = {}, refetch } = useQuery(APP_INIT_QUERY, {
		variables: {
			endsAtLocation,
			fullUrnWithoutPassageCitation: urnWithoutPassageCitation,
			startsAtLocation,
			textGroupUrn: textGroupUrn(urn),
		},
	});

	const {
		loading: userGeneratedContentLoading,
		error: userGeneratedContentError,
		data: userGeneratedContentData,
		refetch: refetchUserGeneratedContent,
	} = useQuery(USER_GENERATED_CONTENT_QUERY, {
		variables: {
			workUrn: urnWithoutPassageCitation,
		},
	});

	const [language, setLanguage] = useState('');

	useEffect(() => {
		let { workByUrn: work } = data || {};

		if (work && work.textNodes) {
			// set language for ltr or rtl
			if (work.language && work.language.title) {
				setLanguage(work.language.title.toLowerCase());
			}

			const [start, end] = getCurrentLocationSpan(work.textNodes);

			updateUrl(start, end, work.full_urn);
			refetch();
		}
	}, [data.workByUrn]);

	useEffect(() => {
		refetchUserGeneratedContent();
	}, [urnWithoutPassageCitation]);

	const openSidePanelForUrn = (selectionUrn, text = '') => {
		dispatch({
			type: SET_SIDE_PANEL_STATE,
			commentFormOpen: Boolean(text),
			selectionText: text,
			selectionUrn,
			sidePanelOpen: true,
			translationFormOpen: !Boolean(text),
		});
	};

	if (loading) {
		return (
			<div className="ReadingEnv">
				<div className="readingEnvLoadingContent">
					<div className="readingEnvLoadingHeader" />
					<div className="readingEnvLoadingLine" />
					<div className="readingEnvLoadingLine" />
					<div className="readingEnvLoadingLine" />
					<div className="readingEnvLoadingLine" />
					<div className="readingEnvLoadingLine" />
				</div>
			</div>
		);
	}

	if (error) {
		console.error(`We encountered an error: ${error.message}`);
		console.trace();
		return null;
	}

	// 404
	if (!data.workByUrn) {
		return (
			<div>
				<NotFound />
			</div>
		);
	}

	let { textGroups: textGroups = [], workByUrn: work = {} } = data;

	const textGroup = textGroups[0];
	const { refsDecls } = work;
	const workAndTextGroup = { work, textGroup };
	const textNodes = (work || {}).textNodes || [];

	// there is a bug in classnames that isn't yet fixed,
	// so we need to do this manually for now
	const classes = [
		'ReadingEnv',
		work?.language.slug,
		{ sidePanelVisible: state.sidePanelOpen },
	].map(c => {
		if (typeof c === 'string') {
			return c
		}

		for (const [k, v] of Object.entries(c)) {
			if (v) return k;
		}

		return null;
	}).filter(i => Boolean(i)).join(' ');

	let commentCounts = {};
	let userTexts = [];

	if (!userGeneratedContentLoading && !userGeneratedContentError) {
		commentCounts = userGeneratedContentData.commentCounts?.results ?? {};
		userTexts = userGeneratedContentData.userTexts?.results ?? [];
	}

	return (
		<div className={classes}>
			<ReadingEnvDispatch.Provider value={dispatch}>
				<Header
					project={project}
					text={workAndTextGroup}
					textNodes={textNodes}
				/>
				<Reader
					sidePanelOpen={state.sidePanelOpen}
					text={workAndTextGroup}
					textNodes={textNodes}
					urn={urn}
				>
					{textNodes.map((textNode, i) => {
						const prevNode = textNodes[i - 1];
						const prevLocation = (prevNode && prevNode.location) || [];
						const { location, urn: textNodeUrn } = textNode;
						const textNodeCommentCount = commentCounts[location.join('.')] || 0;

						let authedOpenSidePanelForUrn;
						if (canEditComments()) {
							authedOpenSidePanelForUrn = openSidePanelForUrn;
						}

						const { heading, subheading, subsubheading } = getHeadings(
							location,
							prevLocation,
							refsDecls,
							textNodeUrn.slice(0, textNodeUrn.lastIndexOf(':')),
							commentCounts,
							authedOpenSidePanelForUrn
						);
						const thisNodeUserTexts =
							userTexts.filter(
								ut =>
									getCitationFromURN(ut.urn) === getCitationFromURN(textNodeUrn)
							);

						return (
							<div key={`div-${textNode.urn}-${textNode.location}`}>
								{heading}
								{subheading}
								{subsubheading}
								<TextNode
									commentCount={textNodeCommentCount}
									edition={thisNodeUserTexts.find(ut => ut.content_type === 'EDITION')}
									hideInteractiveButtons={!canEditComments()}
									textNode={textNode}
									translation={thisNodeUserTexts.find(ut => ut.content_type === 'TRANSLATION')}
									urn={urn}
								/>
							</div>
						);
					})}
				</Reader>
				<SidePanel
					commentFormOpen={state.commentFormOpen}
					currentComment={state.currentComment}
					currentTranslation={state.currentTranslation}
					refetchComments={refetchUserGeneratedContent}
					refetchTranslations={refetchUserGeneratedContent}
					selectionText={state.selectionText}
					selectionUrn={state.selectionUrn}
					showTranslationForm={state.translationFormOpen}
					sidePanelOpen={state.sidePanelOpen}
					text={workAndTextGroup}
				/>
				<RevisionsModal
					open={state.revisionsModalOpen}
					refetch={refetch}
					revisable={state.revisionsModalCurrentRevisable}
				/>
			</ReadingEnvDispatch.Provider>
		</div>
	);
};

App.propTypes = {
	project: PropTypes.object,
	urn: PropTypes.string.isRequired,
};

export default App;
