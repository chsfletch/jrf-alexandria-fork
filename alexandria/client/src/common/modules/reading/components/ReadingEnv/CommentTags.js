/**
 * @prettier
 */

import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Set } from 'immutable';
import _s from 'underscore.string';

import canEditComments from '../../lib/canEditComments';
import makeRequest from '../../lib/makeRequest';

function Tag({ commentId, name, id, removeTag, editable }) {
	const deleteTag = useCallback(
		e => {
			e.preventDefault();
			e.stopPropagation();

			if (commentId) {
				makeRequest(`comments/${commentId}/remove_tag/?name=${name}`, {
					method: 'DELETE',
				});
			}
			removeTag(name);
		},
		[commentId, name]
	);

	return (
		<div className="bg-gray-lighter br gray-darker inline-block commentTag">
			{(editable && canEditComments()) ? (
				<a className="mt1 pointer" onClick={deleteTag}>
					{name}
					&nbsp; &times;
				</a>
			) : (
				<a className="mt1 pointer" href={`/tags/${id}/${_s.slugify(name)}`}>{name}</a>
			)}
		</div>
	);
}

Tag.propTypes = {
	id: PropTypes.number,
	commentId: PropTypes.number,
	name: PropTypes.string,
	removeTag: PropTypes.func,
	editable: PropTypes.bool,
};

function CommentTags({ commentId, removeTag, tags, editable }) {
	return (
		<div className="commentTags">
			{tags.map((tag = {}, i) => {
				if (tag && tag.name) {
					const { name, id } = tag;

					return (
						<Tag
							commentId={commentId}
							key={`${name}-${i}`}
							name={name}
							id={id}
							removeTag={removeTag}
							editable={editable}
						/>
					);
				}
				return null;
			})}
		</div>
	);
}

CommentTags.propTypes = {
	commentId: PropTypes.number,
	removeTag: PropTypes.func,
	editable: PropTypes.bool,
	tags: PropTypes.oneOfType([PropTypes.array, PropTypes.instanceOf(Set)]),
};

export default CommentTags;
