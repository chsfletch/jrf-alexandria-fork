/**
 * @flow
 * @prettier
 */

import * as React from 'react';
import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import LinearProgress from '@material-ui/core/LinearProgress';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import { gql, useQuery } from '@apollo/client';

import SidePanelText from '#common/modules/reading/components/ReadingEnv/SidePanel/SidePanelText';

const EDITIONS_QUERY = gql`
	query editionsQuery($urn: CtsUrn) {
		works(urn: $urn) {
			id
			exemplar {
				id
				title
				description
				urn
			}
			version {
				id
				title
				description
				urn
			}
			work_type
		}
	}
`;

type SidePanelEditionsProps = {
	urn: string,
};

const useStyles = makeStyles(theme => ({
	formControl: {
		margin: 0,
		minWidth: 120,
		maxWidth: '80%',
		width: '100%',
		padding: '0 0 1rem 0',
	},
	panelText: {
		padding: '0 0 3rem 0',
	},
}));

export default function SidePanelEditions({
	urn,
}: SidePanelEditionsProps): React.Node {
	const classes = useStyles();
	const [value, setValue] = useState('');
	const handleChange = evt => setValue(evt.target.value);
	const { loading, error, data } = useQuery(EDITIONS_QUERY, {
		variables: { urn },
	});

	if (loading) return <LinearProgress />;

	if (error)
		return <p className="error">Sorry, we encountered an error: {error}</p>;

	const editions = (data.works || [])
		.filter(
			w =>
				w.work_type === 'edition' && (w.exemplar !== null || w.version !== null)
		)
		.map(w => w.exemplar || w.version);

	return (
		<div className={`sidePanelText ${classes.panelText}`}>
			<FormControl className={classes.formControl}>
				<InputLabel id="edition-select-label">Editions</InputLabel>
				<Select
					id="edition-select"
					labelId="edition-select-label"
					onChange={handleChange}
					value={value}
					width={600}
				>
					{editions.length === 0 && (
						<p className="error">
							Sorry, we could&apos;t find any editions for that work.
						</p>
					)}
					{editions.map((t, i) => {
						return (
							<MenuItem key={`${t.id}-${i}`} value={t.id}>
								{t.title}
							</MenuItem>
						);
					})}
				</Select>
			</FormControl>
			{Boolean(value) ?
				<SidePanelText id={value} />
			:
				<p>Please select an edition.</p>
			}
		</div>
	);
}
