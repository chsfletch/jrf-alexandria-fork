/**
 * @flow
 * @prettier
 */

import * as React from 'react';
import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import prune from 'underscore.string/prune';
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import LinearProgress from '@material-ui/core/LinearProgress';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import { gql, useLazyQuery, useQuery } from '@apollo/client';

import getUrnFromUrl from '#common/modules/reading/lib/getUrnFromUrl';
import renderCanonicalTextNodes from '#client/lib/renderCanonicalTextNodes';
import renderUserTextNodes from '#client/lib/renderUserTextNodes';

function Error({ error }) {
	return (
		<div>
			<span className="error">
				Sorry, we encountered an error. {error.message}
			</span>
		</div>
	);
}

Error.propTypes = {
	error: PropTypes.shape({
		message: PropTypes.string,
	})
};

function useAlexandriaApi(urn: string) {
	const [userTexts, setUserTexts] = useState([]);
	const [userTextsError, setUserTextsError] = useState(null);
	const [userTextsLoading, setUserTextsLoading] = useState(true);

	useEffect(() => {
		fetch(
			`/api/usertexts/?urn_search=${encodeURIComponent(
				getUrnFromUrl()
			)}&content_type=translation`
		)
			.then(res => res.json())
			.then(res => setUserTexts(res.results))
			.catch(err => setUserTextsError(err))
			.finally(() => setUserTextsLoading(false));
	}, [urn]);

	return { userTexts, error: userTextsError, loading: userTextsLoading };
}

function UserTextTranslation({ urn }): React.Node {
	const { error, loading, userTexts } = useAlexandriaApi(urn);

	if (!urn) {
		return null;
	}

	if (loading) {
		return <LinearProgress />;
	}

	if (error) {
		return <Error error={error} />;
	}

	if (!userTexts || userTexts.length === 0) {
		return <p className="error">Sorry, we couldn&apos;t find any translations for this passage.</p>;
	}

	return <div className="editor">{renderUserTextNodes(userTexts)}</div>;
}

UserTextTranslation.propTypes = {
	urn: PropTypes.string,
};

export const TRANSLATION_TEXT_NODES_QUERY = gql`
	query translationTextNodesQuery($workId: Int) {
		work(id: $workId) {
			textNodes {
				id
				index
				location
				text
				urn
			}
		}
	}
`;

type SidePanelTranslationProps = {
	workId: string,
};

function SidePanelTranslation({ workId }: SidePanelTranslationProps): React.Node {
	const [getTextNodes, { called, data, error, loading }] = useLazyQuery(TRANSLATION_TEXT_NODES_QUERY, {
		variables: { workId }
	});

	useEffect(() => {
		if (!workId) return;

		getTextNodes();
	}, [workId]);

	if (!called) {
		return <p>Please select a translation.</p>;
	}

	if (called && loading) {
		return <p>Loading translation...</p>;
	}

	if (error) {
		return <Error error={error} />;
	}

	return <div>{renderCanonicalTextNodes(data.work.textNodes)}</div>;
}

SidePanelTranslation.propTypes = {
	workId: PropTypes.string,
};

export const TRANSLATIONS_QUERY = gql`
	query translationsQuery($urn: CtsUrn) {
		works(urn: $urn) {
			id
			translation {
				id
				title
				description
				urn
			}
		}
	}
`;

type SidePanelTranslationsProps = {
	urn: string,
};

const useStyles = makeStyles(theme => ({
	formControl: {
		margin: 0,
		minWidth: 120,
		maxWidth: '80%',
		width: '100%',
		padding: '0 0 1rem 0',
	},
	panelText: {
		padding: '0 0 3rem 0',
	},
}));

const PROJECT_TRANSLATION_VALUE = 'my-project-translation';

export default function SidePanelTranslations({ urn }: SidePanelTranslationsProps): React.Node {
	const classes = useStyles();
	const [value, setValue] = useState('');
	const handleChange = evt => setValue(evt.target.value);
	const { loading, error, data } = useQuery(TRANSLATIONS_QUERY, {
		variables: { urn }
	});

	if (loading) return <LinearProgress />

	if (error) return <p className="error">Sorry, we encountered an error: {error}</p>;

	const translationWorks = (data.works || []).filter(w => w.translation !== null);

	return (
		<div className={`sidePanelText ${classes.panelText}`}>
			<FormControl className={classes.formControl}>
				<InputLabel id="translation-select-label">Translations</InputLabel>
				<Select
					autoWidth
					id="translation-select"
					labelId="translation-select-label"
					onChange={handleChange}
					value={value}

				>
					{translationWorks.map((t, i) => {
						return (
							<MenuItem key={`${t.id}-${i}`} value={t.id.toString()}>{prune(t.translation.title, 60)}</MenuItem>
						);
					})}
					<MenuItem key={PROJECT_TRANSLATION_VALUE} value={PROJECT_TRANSLATION_VALUE}>This project&apos;s translation</MenuItem>
				</Select>
			</FormControl>
			{value === PROJECT_TRANSLATION_VALUE ? <UserTextTranslation urn={urn} /> : <SidePanelTranslation workId={value} />}
		</div>
	);
}
