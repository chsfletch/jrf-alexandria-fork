/**
 * @prettier
 */

import React, {
	createRef,
	useContext,
	useEffect,
	useLayoutEffect,
	useMemo,
	useState,
} from 'react';
import PropTypes from 'prop-types';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';
import EditIcon from '@material-ui/icons/Edit';
import HistoryIcon from '@material-ui/icons/History';
import ModeCommentTwoToneIcon from '@material-ui/icons/ModeCommentTwoTone';
import PublishOutlinedIcon from '@material-ui/icons/PublishOutlined';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import TranslateIcon from '@material-ui/icons/Translate';
import cn from 'classnames';
import {
	ContentState,
	EditorState,
	convertFromHTML,
	convertFromRaw,
	convertToRaw,
} from 'draft-js';

import OrpheusEditor from '#common/modules/orpheus-editor/components/Editor';
import decorators from '#common/modules/orpheus-editor/components/decorators';

import deriveLocationFromUrn from '#common/modules/reading/lib/deriveLocationFromUrn';
import makeRequest from '#common/modules/reading/lib/makeRequest';

import {
	ReadingEnvDispatch,
	SET_REVISIONS_MODAL_STATE,
	SET_SIDE_PANEL_STATE
} from '#common/modules/reading/constants';

const noop = () => {};

const withoutPassageCitation = (urn = '') =>
	urn
		.split(':')
		.slice(0, 4)
		.join(':');

const _createUserText = async userText => {
	return await makeRequest('usertexts/', {
		body: JSON.stringify(userText),
		method: 'POST',
	});
};

const _updateUserText = async userText => {
	return await makeRequest(`usertexts/${userText.id}/`, {
		body: JSON.stringify(userText),
		method: 'PUT',
	});
};

const renderCommentsIcon = (commentCount = 0, onClick = () => {}) => {
	const classes = cn(
		'f2 flex flex-column items-center pointer relative unselectable',
		{
			hidden: commentCount === 0,
		}
	);
	return (
		<div className={classes} onClick={onClick}>
			<div
				className="absolute left unselectable z2 commentIconCount"
				style={{
					fontWeight: 'bold',
					fontSize: 12,
					left: '50%',
					paddingBottom: 4,
					top: '50%',
					transform: 'translate(-50%, -50%)',
					fontFamily:
						'-apple-system, BlinkMacSystemFont, Helvetica Neue, Helvetica, Liberation Sans, Noto, sans-serif',
				}}
			>
				{commentCount}
			</div>
			<ModeCommentTwoToneIcon fontSize="inherit" />
		</div>
	);
};

const renderEditingIcon = (
	editing,
	classes,
	handleEditClick,
	handleCancelClick,
	handleSubmitClick
) => {
	if (editing) {
		return (
			<div>
				<div className={cn(classes, 'mr1')} onClick={handleSubmitClick}>
					<CheckIcon />
				</div>
				<div className={cn(classes, 'mr1')} onClick={handleCancelClick}>
					<CloseIcon />
				</div>
			</div>
		);
	}

	return (
		<div className={cn(classes, 'mr1')} onClick={handleEditClick}>
			<EditIcon />
		</div>
	);
};

const UserEditionTextNode = ({
	editing,
	editorState,
	handleCancel,
	handleSubmit,
	setEditorState,
	textNode,
}) => {
	const editorNode = createRef();

	useEffect(() => {
		if (editing) {
			try {
				editorNode.current.focus();
			} catch (e) {
				console.error('Failed to focus OrpheusEditor. Reason:', e.toString());
			}
		}
	}, [editing]);

	const classes = cn({
		editing: editing,
		p1: editing,
	});

	return (
		<div className={classes}>
			<OrpheusEditor
				editorKey={textNode.id}
				editorState={editorState}
				handleChange={setEditorState}
				readOnly={!editing}
				ref={editorNode}
			/>
		</div>
	);
};

UserEditionTextNode.propTypes = {
	editing: PropTypes.bool,
	editorState: PropTypes.object,
	handleCancel: PropTypes.func,
	handleSubmit: PropTypes.func,
	userText: PropTypes.shape({
		latest_revision_text: PropTypes.string,
		revisions: PropTypes.array,
	}),
	setEditorState: PropTypes.func,
	textNode: PropTypes.shape({
		id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
		text: PropTypes.string,
	}),
};

// https://github.com/facebook/draft-js/issues/586#issuecomment-300347678
// which was inspired by https://github.com/facebook/draft-js/blob/master/src/model/paste/getSafeBodyFromHTML.js
const simpleDOMBuilder = html => {
	// only load JSDOM and set these globals if we're on the server
	if (typeof document === 'undefined') {
		const jsdom = require('jsdom');
		const dom = new jsdom.JSDOM(`<!DOCTYPE html>`);
		const _window = dom.window;

		global.document = _window.document;
		global.HTMLElement = _window.HTMLElement;
		global.HTMLAnchorElement = _window.HTMLAnchorElement;
		global.Node = _window.Node;
	}

  const doc = document.implementation.createHTMLDocument('mydoc');
  doc.documentElement.innerHTML = html;
  const root = doc.getElementsByTagName('body')[0];
  return root;
}

function getBaseContentFromEdition(textNode, userText) {
	let baseContent;

	const latestRevision = userText.latest_revision_text || userText.revisions[0] && userText.revisions[0].text;
	if (!latestRevision) return null;

	try {
		baseContent = convertFromRaw(JSON.parse(latestRevision));
	} catch (e) {
		const { contentBlocks, entityMap } = convertFromHTML(
			latestRevision,
			simpleDOMBuilder
		);
		baseContent = ContentState.createFromBlockArray(contentBlocks, entityMap);
	}

	return baseContent;
}

function getBaseEditorState(textNode, edition) {
	let baseContent;

	if (edition.content_type === 'EDITION') {
		baseContent = getBaseContentFromEdition(textNode, edition);
	} else {
		// FIXME: (charles) using convertFromHTML() is causing
		// editor-key mismatches on the client and server. We have two
		// options: don't use Draft.js except for editing text (make
		// it client-side only); pre-convert all plain text into
		// Draft.js raw ContentState format. Long-term, I think option 1 is best,
		// but for now we should maybe setup a conversion task.

		// I tried assuming no rich formatting, but that still doesn't
		// work: baseContent = ContentState.createFromText(textNode.text);

		const { contentBlocks, entityMap } = convertFromHTML(
			textNode.text,
			simpleDOMBuilder
		);
		baseContent = ContentState.createFromBlockArray(contentBlocks, entityMap);
	}

	const baseEditorState = EditorState.createWithContent(
		baseContent,
		decorators
	);

	return baseEditorState;
}

const TextNode = ({
	commentCount,
	edition = {},
	hideInteractiveButtons,
	textNode,
	translation = {},
	urn,
}) => {
	const location = textNode.location || deriveLocationFromUrn(urn);
	urn = `${withoutPassageCitation(urn)}:${location.join('.')}`;
	const [localUserText, setLocalUserText] = useState(edition);
	const [documentHasSelection, setDocumentHasSelection] = useState(false);
	const [editing, setEditing] = useState(false);
	const [editorState, setEditorState] = useState(
		getBaseEditorState(textNode, edition)
	);
	// editor state to revert to on cancel
	const [baseEditorState, setBaseEditorState] = useState(editorState);

	useEffect(() => {
		setDocumentHasSelection(!window.getSelection().isCollapsed);
	}, [typeof window !== 'undefined' && window.getSelection().isCollapsed]);

	useEffect(() => {
		const newEditorState = getBaseEditorState(textNode, edition);
		setEditorState(newEditorState);
		setBaseEditorState(newEditorState);
	}, [JSON.stringify(edition)]);

	const dispatch = useContext(ReadingEnvDispatch);

	if (editorState === null) {
		return null;
	}

	const openSidePanelForTranslation = (selectionUrn, selectionText) => {
		dispatch({
			type: SET_SIDE_PANEL_STATE,
			currentTranslation: translation,
			selectionText,
			selectionUrn,
			sidePanelOpen: true,
			translationFormOpen: true,
		})
	};

	const openSidePanelForUrn = (selectionUrn, selectionText) => {
		dispatch({
			type: SET_SIDE_PANEL_STATE,
			selectionText,
			selectionUrn,
			sidePanelOpen: true,
		});
	};

	const createUserText = rawContent => {
		const revision = {
			text: rawContent,
		};

		const newUserText = {
			// Django appears to use caps by convention
			// see https://docs.djangoproject.com/en/3.0/ref/models/fields/#enumeration-types
			content_type: 'EDITION',
			revisions: [revision],
			urn,
		};

		return _createUserText(newUserText);
	};

	const updateUserText = rawContent => {
		const currentEdition = Object.assign({}, localUserText, {
			content_type: 'EDITION',
		});
		const now = new Date();
		const revision = {
			created_at: now,
			text: rawContent,
			updated_at: now,
		};

		currentEdition.revisions = [...(currentEdition.revisions || []), revision];

		return _updateUserText(currentEdition);
	};

	const handleCommentClick = e => {
		e.preventDefault();

		openSidePanelForUrn(urn, editorState.getCurrentContent().getPlainText());
	};

	const handleCancelEdit = e => {
		e.preventDefault();

		setEditing(false);
		setEditorState(baseEditorState);
	};

	const handleEditClick = e => {
		e.preventDefault();

		setEditing(true);
		setBaseEditorState(editorState);
	};

	const handleSubmitEdit = async e => {
		e.preventDefault();

		let content;
		try {
			content = JSON.stringify(convertToRaw(editorState.getCurrentContent()));
		} catch (e) {
			console.error('Could not convert content to raw. Reason:', e);
			return;
		}
		const result = Boolean((localUserText || {}).id)
			? await updateUserText(content)
			: await createUserText(content);

		if (Boolean(result)) {
			setEditing(false);
			setLocalUserText(result);
		}
	};

	const handleRevisionHistoryClick = e => {
		e.preventDefault();

		dispatch({
			type: SET_REVISIONS_MODAL_STATE,
			revisionsModalCurrentRevisable: edition,
			revisionsModalOpen: true,
		});
	};

	const handleTranslateClick = e => {
		e.preventDefault();
		const textForTranslation = editorState.getCurrentContent().getPlainText();
		openSidePanelForTranslation(urn, textForTranslation);
	};

	const showCommentsForNode = _e => {
		openSidePanelForUrn(urn);
	};

	const inlineButtonClasses = useMemo(() => cn(
		'child ease-in-out fw7 gray-dark pointer unselectable mr1',
		{
			hidden: documentHasSelection,
		}
	), [documentHasSelection]);
	const commentButtonClasses = useMemo(() => cn(
		'ease-in-out fw7 pointer unselectable commentButton',
		{
			hidden: documentHasSelection,
		}
	), [documentHasSelection]);

	const textClasses = cn('textNode w-80', {
		indent: textNode.text.indexOf('<milestone') > -1,
	});

	// render empty line for empty text
	if (textNode.text === ""){
		textNode.text = "[...]";
	}

	return (
		<div
			className="flex hide-child"
			style={{ fontSize: '1.25rem', lineHeight: '1.6em' }}
		>
			<div className="flex flex-row items-center textNodeLocation">
				<div
					className="child ease-in-out fw7 gray-dark left pointer unselectable"
					onClick={hideInteractiveButtons ? noop : handleCommentClick}
					role="button"
					style={{ maxWidth: 70, width: 70 }}
				>
					{location.join('.')}
				</div>
			</div>
			<div
				className={textClasses}
				data-urn={urn}
				data-location={JSON.stringify(location)}
				style={{ flexGrow: 1 }}
			>
				<UserEditionTextNode
					editing={editing}
					editorState={editorState}
					handleCancel={handleCancelEdit}
					handleSubmit={handleSubmitEdit}
					setEditorState={setEditorState}
					textNode={textNode}
				/>
			</div>
			<div
				className="flex flex-row items-center px1 relative"
				style={{ top: 4 }}
			>
				{!hideInteractiveButtons &&
					renderEditingIcon(
						editing,
						inlineButtonClasses,
						handleEditClick,
						handleCancelEdit,
						handleSubmitEdit
					)}
				{!hideInteractiveButtons && (
					<React.Fragment>
						<div
							className={inlineButtonClasses}
							onClick={handleTranslateClick}
							role="button"
						>
							<TranslateIcon />
						</div>
						{edition.revisions && edition.revisions.length > 0 && <div
							alt="Revision history"
							className={inlineButtonClasses}
							onClick={handleRevisionHistoryClick}
							role="button"
						>
							<HistoryIcon />
						</div>}
					</React.Fragment>
				)}
				<div className={commentButtonClasses}>
					{renderCommentsIcon(commentCount, showCommentsForNode)}
				</div>
			</div>
		</div>
	);
};

TextNode.propTypes = {
	commentCount: PropTypes.number,
	edition: PropTypes.shape({
		content_type: PropTypes.oneOf(['EDITION']),
		latest_revision_text: PropTypes.string
	}),
	hideInteractiveButtons: PropTypes.bool,
	textNode: PropTypes.shape({
		location: PropTypes.array,
		text: PropTypes.string,
		urn: PropTypes.string,
	}).isRequired,
	translation: PropTypes.shape({
		content_type: PropTypes.oneOf(['TRANSLATION']),
		latest_revision_text: PropTypes.string
	}),
	urn: PropTypes.string,
};

export default TextNode;
export { renderCommentsIcon };
