/**
 * @flow
 * @prettier
 */

import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import CommentForm from '../CommentForm';
import SidePanelContent from './SidePanelContent';
import TranslationForm from '../TranslationForm';

import { ReadingEnvDispatch, SET_SIDE_PANEL_STATE } from '../../../constants';

type SidePanelProps = {
	commentFormOpen: bool,
	currentComment: {
		id: number,
		latestRevision: {
			text: string,
			title: string,
		},
	},
	currentTranslation: {
		id: number
	},
	refetchComments: () => void,
	refetchTranslations: () => void,
	selectionText: string,
	selectionUrn: string,
	showTranslationForm: bool,
	sidePanelOpen: bool,
	text: Object,
};

const SidePanel = ({
	commentFormOpen,
	currentComment = {},
	currentTranslation = {},
	refetchComments,
	refetchTranslations,
	selectionText,
	selectionUrn,
	showTranslationForm,
	sidePanelOpen,
	text,
}: SidePanelProps) => {
	const dispatch = useContext(ReadingEnvDispatch);

	const openCommentForm = e => {
		dispatch({
			type: SET_SIDE_PANEL_STATE,
			commentFormOpen: true,
		});
	};

	const toggleSidePanel = e => {
		dispatch({
			type: SET_SIDE_PANEL_STATE,
			sidePanelOpen: !sidePanelOpen,
		});
	};

	const chevronClasses = classnames('mdi', {
		'mdi-chevron-right': sidePanelOpen,
		'mdi-chevron-left': !sidePanelOpen,
	});

	const sidePanelClasses = classnames('sidePanelOuter', {
		'-sidePanelOpen': sidePanelOpen
	})

	return (
		<div className={sidePanelClasses}>
			<div className="sidePanelHandle" onClick={toggleSidePanel}>
				<div className="sidePanelHandleBarGrip">
					<i className={chevronClasses} />
				</div>
			</div>
			{sidePanelOpen && (
				<div className="sidePanel hide-scrollbars">
					{commentFormOpen && (
						<div className="sidePanelForm">
							<CommentForm
								comment={currentComment || {}}
								commentTarget={selectionText}
								key={(currentComment || {}).id || selectionText}
								refetchComments={refetchComments}
								urn={selectionUrn}
							/>
						</div>
					)}
					{showTranslationForm && (
						<div className="sidePanelForm">
							<TranslationForm
								key={(currentTranslation || {}).id || selectionText}
								refetchTranslations={refetchTranslations}
								textForTranslation={selectionText}
								translation={currentTranslation || {}}
								urn={selectionUrn}
							/>
						</div>
					)}
					{!commentFormOpen && !showTranslationForm && (
						<SidePanelContent
							focusedUrn={selectionUrn}
							openCommentForm={openCommentForm}
							refetchComments={refetchComments}
							text={text}
						/>
					)}
				</div>
			)}
		</div>
	);
};

SidePanel.propTypes = {
	commentFormOpen: PropTypes.bool,
	currentComment: PropTypes.shape({
		id: PropTypes.number,
		latestRevision: PropTypes.shape({
			text: PropTypes.string,
			title: PropTypes.string,
		}),
	}),
	currentTranslation: PropTypes.shape({ id: PropTypes.number }),
	refetchComments: PropTypes.func,
	refetchTranslations: PropTypes.func,
	selectionText: PropTypes.string,
	selectionUrn: PropTypes.string,
	showTranslationForm: PropTypes.bool,
	sidePanelOpen: PropTypes.bool,
	text: PropTypes.object,
};

export default SidePanel;
