/**
 * @prettier
 */

import { hot } from 'react-hot-loader/root';

import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter, Route } from 'react-router-dom';

import ReadingContainer from '#common/reading/containers/ReadingContainer';
import Root from '#common/containers/Root';

const ReadingEnvironmentView = ({ match }) => (
  <Root>
    <BrowserRouter>
   		<Route path={`${match.url}/:urn`} component={ReadingContainer} />
    </BrowserRouter>
  </Root>
);

ReadingEnvironmentView.propTypes = {
	urn: PropTypes.string.isRequired,
};

const ReadingEnv = <Route path="/read" component={ReadingEnvironmentView} />

export default hot(ReadingEnv);
