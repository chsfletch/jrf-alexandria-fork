import faker from 'faker';
import titleize from 'underscore.string/titleize';


import { mockFiles } from '../../../../mocks/files';

const mockCover = () => {
	const cover = {};

	cover.title = titleize(faker.lorem.words());
	cover.label = titleize(faker.lorem.words());
	cover.lead = faker.lorem.sentences();
	cover.buttonText = titleize(faker.lorem.words());
	cover.buttonUrl = '/';

	const files = mockFiles(0, 1);

	if (files.length) {
		const file = files[0];
		cover.imageUrl = `//iiif.orphe.us/${file.name}/full/full/0/default.jpg`;
	}

	return cover;
};

export default mockCover;
