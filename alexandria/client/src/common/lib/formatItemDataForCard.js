import _s from 'underscore.string';

const formatItemDataForCard = (item) => {
	const itemUrl = `/items/${item._id}/${item.slug}`;
	const metadata = [];
	let files = [];
	let file;
	let imageUrl;
	let audioSrc;
	let videoSrc;
	let pdfSrc;
	let mediaUrl;

	if (item.files && item.files.length) {
		files = item.files;
	}

	if (files.length) {
		file = files[0];
		const fileType = file.type || '';
		const isImage = fileType.slice(0, fileType.indexOf('/')) === 'image';

		if (isImage) {
			imageUrl = `//iiif.orphe.us/${file.name}/full/full/0/default.jpg`;
			// for background image files uploaded to s3 in dashboard.project_settings
			if (file.name.includes('https')) {
				imageUrl = file.name;
			}
		} else {
			// TODO: handle media
			if (file.type.indexOf("audio") >= 0) {
				audioSrc = `https://s3.amazonaws.com/iiif-orpheus/${file.name}`;
			} else if (file.type.indexOf("video") >= 0){
				videoSrc = `https://s3.amazonaws.com/iiif-orpheus/${file.name}`;
			} else if (file.type.indexOf("pdf") >= 0){
				pdfSrc = `https://s3.amazonaws.com/iiif-orpheus/${file.name}`;
			} else {
				mediaUrl = `https://s3.amazonaws.com/iiif-orpheus/${file.name}`;
			}
		}
	}

	if (item.metadata && item.metadata.length) {
		item.metadata.forEach(datum => {
			if (datum.type.indexOf('text', 'number') >= 0) {
				metadata.push({
					type: datum.label,
					metaLabel: datum.label,
					metaValue: datum.value,
				});
			}
		});
	}

	return {
		itemUrl,
		metadata,
		files,
		file,
		imageUrl,
		audioSrc,
		videoSrc,
		pdfSrc,
		mediaUrl,
	};
};

export default formatItemDataForCard;
