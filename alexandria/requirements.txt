django-sitemaps==1.1.1

aniso8601==7.0.0
attrs==19.3.0
boto3==1.12.24
botocore==1.15.31
certifi==2019.11.28
chardet==3.0.4
coverage==5.0.4
dataclasses==0.6
Django==2.2.6
django-annoying==0.10.6
django-ckeditor==5.9.0
django-cors-headers==3.1.1
django-extensions==2.2.8
django-filter==2.2.0
django-jet==1.0.8
django-js-asset==1.2.2
django-jsonstore==0.4.1
django-material==1.6.3
django-rest-auth==0.9.5
django-storages==1.9.1
django-summernote==0.8.11.6
django-tinymce==2.8.0
django-viewflow==1.6.0
django-webpack-loader==0.6.0
djangorestframework==3.11.0
docutils==0.15.2
gql==2.0.0
graphene==2.1.8
graphene-django==2.8.0
graphql-core==2.3.2
graphql-relay==2.0.1
gunicorn==20.0.4
httpretty==1.1.3
idna==2.9
importlib-metadata==1.6.0
jmespath==0.9.5
more-itertools==8.2.0
packaging==20.3
Pillow==8.0.0
pluggy==0.13.1
promise==2.3
psycopg2==2.8.6
py==1.8.1
pyparsing==2.4.7
pytest==5.4.1
python-dateutil==2.8.1
python-slugify==4.0.0
pytz==2019.3
requests==2.23.0
rules==2.2
Rx==1.6.1
s3transfer==0.3.3
singledispatch==3.4.0.3
six==1.14.0
sqlparse==0.2.4
unidecode==1.2.0
urllib3==1.25.8
wcwidth==0.1.9
werkzeug==1.0.1
zipp==3.1.0
