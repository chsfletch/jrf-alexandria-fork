# environment dependent settings
import os

# this enables pre-built static files to be located on production server
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
STATIC_ROOT=os.getenv('STATIC_ROOT', os.path.join(BASE_DIR, "staticfiles"))

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "alexandria_app/static/"),
    os.path.join(BASE_DIR, "client/build/static/"),
    # Add these last two for parcel in development
    # os.path.join(BASE_DIR, "client/dist/"),
    # os.path.join(BASE_DIR, "client/node_modules/"),
]

# database config
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv('DATABASE_NAME', 'NAME_OF_YOUR_DB'),
        'USER': os.getenv('DATABASE_USER', 'USER_OF_YOUR_DB'),
        'PASSWORD': os.getenv('DATABASE_PASSWORD', 'PASSWORD_OF_YOUR_DB'),
        'HOST': os.getenv('DATABASE_HOST', '127.0.0.1'),
    }
}

# email config
SENDGRID_API_KEY = os.getenv('SENDGRID_API_KEY')
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER', 'apikey')
EMAIL_HOST_PASSWORD = SENDGRID_API_KEY
EMAIL_PORT = 587
EMAIL_USE_TLS = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('SECRET_KEY', 'YOUR_SECRET')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.getenv('DEBUG', False)

# media storage to s3
AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID', 'key')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY', 'sec')
AWS_STORAGE_BUCKET_NAME = os.getenv('AWS_STORAGE_BUCKET_NAME', 'bucket')
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}
AWS_LOCATION = os.environ['AWS_LOCATION'] if 'AWS_LOCATION' in os.environ else 'media'
AWS_S3_FILE_OVERWRITE = False

AWS_MEDIA_URL = F'https://{AWS_STORAGE_BUCKET_NAME}.s3.amazonaws.com/{AWS_LOCATION}/'

# URL of Textserver
TEXTSERVER = os.getenv('TEXTSERVER',
                       'http://text.chs.harvard.edu/graphql')
# URL for text upload endpoint of Textserver
TEXTSERVER_TEXT = os.getenv('TEXTSERVER_TEXT', 'http://text.chs.harvard.edu/text')

# subdomain cookies
# do not set any subdomain cookies because we're supporting custom domain names now (homer.chs.harvard.edu)
# SESSION_COOKIE_DOMAIN = os.getenv('SESSION_COOKIE_DOMAIN', '.alexandria.local')
DOMAIN_NAME = os.getenv('DOMAIN_NAME', 'alexandria.local')
RENDERING_SERVER_URL = os.getenv('RENDERING_SERVER_URL',
                                 'http://127.0.0.1:8082')
